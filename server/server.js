const MongoClient = require('mongodb').MongoClient
const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const mailer = require('nodemailer')
const fs = require('fs')
const https = require('https')
const http = require('http')
const req = require('request')

const saveToDb = require('./SaveToDB')
const API_PORT = 9000
const app = express()
const router = express.Router()

// ========= Certificate SSL ===========
const privateKey = fs.readFileSync('/etc/letsencrypt/live/snp100.com/privkey.pem', 'utf8')
const certificate = fs.readFileSync('/etc/letsencrypt/live/snp100.com/cert.pem', 'utf8')
const ca = fs.readFileSync('/etc/letsencrypt/live/snp100.com/chain.pem', 'utf8')

const credentials = {
  key: privateKey,
  cert: certificate,
  ca: ca
}

// ====== DB ========
const dbRoute = 'mongodb://localhost:27017/'
const dbName = 'snp100'

// ====== API ======
// S&P key for email snp100.web@gmail.com password: SNPSNP100
const apiBaseUrl = 'https://forex.1forge.com/1.0.3/'
const API_KEY = 'ONSW5JROgcFmgb2facc5OqoCHhpMWtvN'
// const endpoint = `quotes?pairs=EURUSD,USDJPY,GBPUSD,USDCAD,USDCHF,AUDUSD,NZDUSD,EURGBP,EURCHF,EURCAD,EURAUD,EURNZD,EURJPY,GBPJPY,CHFJPY,CADJPY,AUDJPY,NZDJPY,GBPCHF,GBPAUD,GBPCAD,GBPNZD,AUDCAD,AUDCHF,AUDNZD,CADCHF,AUDSGD,CHFSGD,EURNOK,EURRUB,EURSEK,EURSGD,EURTRY,EURZAR,GBPHKD,GBPNOK,GBPSEK,GBPSGD,NOKJPY,NZDCAD,NZDCHF,SGDJPY,USDCNH,USDDKK,USDHKD,USDMXN,"USDNOK","USDPLN","USDRUB","USDSEK","USDSGD","USDTRY","USDZAR","XAGEUR","XAGUSD","XAUEUR","XAUUSD","BTCUSD","ETHBTC","ETHUSD","LTCBTC","LTCUSD","XRPUSD","XRPBTC","DSHUSD","DSHBTC","BCHUSD","BCHBTC","EURHKD","EURCNH","EURDKK","EURMXN","EURPLN","EURXAG","EURXAU","EURBTC","EURETH","EURLTC","EURXRP","EURDSH","EURBCH","USDEUR","USDGBP","USDAUD","USDNZD","USDXAG","USDXAU","USDBTC","USDETH","USDLTC","USDXRP","USDDSH","USDBCH","JPYEUR","JPYUSD","JPYGBP","JPYCAD","JPYCHF","JPYAUD","JPYNZD","JPYSGD","JPYNOK","JPYRUB","JPYSEK","JPYTRY","JPYZAR","JPYHKD","JPYCNH","JPYDKK","JPYMXN","JPYPLN","JPYXAG","JPYXAU","JPYBTC","JPYETH","JPYLTC","JPYXRP","JPYDSH","JPYBCH","GBPEUR","GBPRUB","GBPTRY","GBPZAR","GBPCNH","GBPDKK","GBPMXN","GBPPLN","GBPXAG","GBPXAU","GBPBTC","GBPETH","GBPLTC","GBPXRP","GBPDSH","GBPBCH","CADEUR","CADUSD","CADGBP","CADAUD","CADNZD","CADSGD","CADNOK","CADRUB","CADSEK","CADTRY","CADZAR","CADHKD","CADCNH","CADDKK","CADMXN","CADPLN","CADXAG","CADXAU","CADBTC","CADETH","CADLTC","CADXRP","CADDSH","CADBCH","CHFEUR","CHFUSD","CHFGBP","CHFCAD","CHFAUD","CHFNZD","CHFNOK","CHFRUB","CHFSEK","CHFTRY","CHFZAR","CHFHKD","CHFCNH","CHFDKK","CHFMXN","CHFPLN","CHFXAG","CHFXAU","CHFBTC","CHFETH","CHFLTC","CHFXRP","CHFDSH","CHFBCH","AUDEUR","AUDGBP","AUDNOK","AUDRUB","AUDSEK","AUDTRY","AUDZAR","AUDHKD","AUDCNH","AUDDKK","AUDMXN","AUDPLN","AUDXAG","AUDXAU","AUDBTC","AUDETH","AUDLTC","AUDXRP","AUDDSH","AUDBCH","NZDEUR","NZDGBP","NZDAUD","NZDSGD","NZDNOK","NZDRUB","NZDSEK","NZDTRY","NZDZAR","NZDHKD","NZDCNH","NZDDKK","NZDMXN","NZDPLN","NZDXAG","NZDXAU","NZDBTC","NZDETH","NZDLTC","NZDXRP","NZDDSH","NZDBCH","SGDEUR","SGDUSD","SGDGBP","SGDCAD","SGDCHF","SGDAUD","SGDNZD","SGDNOK","SGDRUB","SGDSEK","SGDTRY","SGDZAR","SGDHKD","SGDCNH","SGDDKK","SGDMXN","SGDPLN","SGDXAG","SGDXAU","SGDBTC","SGDETH","SGDLTC","SGDXRP","SGDDSH","SGDBCH","NOKEUR","NOKUSD","NOKGBP","NOKCAD","NOKCHF","NOKAUD","NOKNZD","NOKSGD","NOKRUB","NOKSEK","NOKTRY","NOKZAR","NOKHKD","NOKCNH","NOKDKK","NOKMXN","NOKPLN","NOKXAG","NOKXAU","NOKBTC","NOKETH","NOKLTC","NOKXRP","NOKDSH","NOKBCH","RUBEUR","RUBUSD","RUBJPY","RUBGBP","RUBCAD","RUBCHF","RUBAUD","RUBNZD","RUBSGD","RUBNOK","RUBSEK","RUBTRY","RUBZAR","RUBHKD","RUBCNH","RUBDKK","RUBMXN","RUBPLN","RUBXAG","RUBXAU","RUBBTC","RUBETH","RUBLTC","RUBXRP","RUBDSH","RUBBCH","SEKEUR","SEKUSD","SEKJPY","SEKGBP","SEKCAD","SEKCHF","SEKAUD","SEKNZD","SEKSGD","SEKNOK","SEKRUB","SEKTRY","SEKZAR","SEKHKD","SEKCNH","SEKDKK","SEKMXN","SEKPLN","SEKXAG","SEKXAU","SEKBTC","SEKETH","SEKLTC","SEKXRP","SEKDSH","SEKBCH","TRYEUR","TRYUSD","TRYJPY","TRYGBP","TRYCAD","TRYCHF","TRYAUD","TRYNZD","TRYSGD","TRYNOK","TRYRUB","TRYSEK","TRYZAR","TRYHKD","TRYCNH","TRYDKK","TRYMXN","TRYPLN","TRYXAG","TRYXAU","TRYBTC","TRYETH","TRYLTC","TRYXRP","TRYDSH","TRYBCH","ZAREUR","ZARUSD","ZARJPY","ZARGBP","ZARCAD","ZARCHF","ZARAUD","ZARNZD","ZARSGD","ZARNOK","ZARRUB","ZARSEK","ZARTRY","ZARHKD","ZARCNH","ZARDKK","ZARMXN","ZARPLN","ZARXAG","ZARXAU","ZARBTC","ZARETH","ZARLTC","ZARXRP","ZARDSH","ZARBCH","HKDEUR","HKDUSD","HKDJPY","HKDGBP","HKDCAD","HKDCHF","HKDAUD","HKDNZD","HKDSGD","HKDNOK","HKDRUB","HKDSEK","HKDTRY","HKDZAR","HKDCNH","HKDDKK","HKDMXN","HKDPLN","HKDXAG","HKDXAU","HKDBTC","HKDETH","HKDLTC","HKDXRP","HKDDSH","HKDBCH","CNHEUR","CNHUSD","CNHJPY","CNHGBP","CNHCAD","CNHCHF","CNHAUD","CNHNZD","CNHSGD","CNHNOK","CNHRUB","CNHSEK","CNHTRY","CNHZAR","CNHHKD","CNHDKK","CNHMXN","CNHPLN","CNHXAG","CNHXAU","CNHBTC","CNHETH","CNHLTC","CNHXRP","CNHDSH","CNHBCH","DKKEUR","DKKUSD","DKKJPY","DKKGBP","DKKCAD","DKKCHF","DKKAUD","DKKNZD","DKKSGD","DKKNOK","DKKRUB","DKKSEK","DKKTRY","DKKZAR","DKKHKD","DKKCNH","DKKMXN","DKKPLN","DKKXAG","DKKXAU","DKKBTC","DKKETH","DKKLTC","DKKXRP","DKKDSH","DKKBCH","MXNEUR","MXNUSD","MXNJPY","MXNGBP","MXNCAD","MXNCHF","MXNAUD","MXNNZD","MXNSGD","MXNNOK","MXNRUB","MXNSEK","MXNTRY","MXNZAR","MXNHKD","MXNCNH","MXNDKK","MXNPLN","MXNXAG","MXNXAU","MXNBTC","MXNETH","MXNLTC","MXNXRP","MXNDSH","MXNBCH","PLNEUR","PLNUSD","PLNJPY","PLNGBP","PLNCAD","PLNCHF","PLNAUD","PLNNZD","PLNSGD","PLNNOK","PLNRUB","PLNSEK","PLNTRY","PLNZAR","PLNHKD","PLNCNH","PLNDKK","PLNMXN","PLNXAG","PLNXAU","PLNBTC","PLNETH","PLNLTC","PLNXRP","PLNDSH","PLNBCH","XAGJPY","XAGGBP","XAGCAD","XAGCHF","XAGAUD","XAGNZD","XAGSGD","XAGNOK","XAGRUB","XAGSEK","XAGTRY","XAGZAR","XAGHKD","XAGCNH","XAGDKK","XAGMXN","XAGPLN","XAGXAU","XAGBTC","XAGETH","XAGLTC","XAGXRP","XAGDSH","XAGBCH","XAUJPY","XAUGBP","XAUCAD","XAUCHF","XAUAUD","XAUNZD","XAUSGD","XAUNOK","XAURUB","XAUSEK","XAUTRY","XAUZAR","XAUHKD","XAUCNH","XAUDKK","XAUMXN","XAUPLN","XAUXAG","XAUBTC","XAUETH","XAULTC","XAUXRP","XAUDSH","XAUBCH","BTCEUR","BTCJPY","BTCGBP","BTCCAD","BTCCHF","BTCAUD","BTCNZD","BTCSGD","BTCNOK","BTCRUB","BTCSEK","BTCTRY","BTCZAR","BTCHKD","BTCCNH","BTCDKK","BTCMXN","BTCPLN","BTCXAG","BTCXAU","BTCETH","BTCLTC","BTCXRP","BTCDSH","BTCBCH","ETHEUR","ETHJPY","ETHGBP","ETHCAD","ETHCHF","ETHAUD","ETHNZD","ETHSGD","ETHNOK","ETHRUB","ETHSEK","ETHTRY","ETHZAR","ETHHKD","ETHCNH","ETHDKK","ETHMXN","ETHPLN","ETHXAG","ETHXAU","ETHLTC","ETHXRP","ETHDSH","ETHBCH","LTCEUR","LTCJPY","LTCGBP","LTCCAD","LTCCHF","LTCAUD","LTCNZD","LTCSGD","LTCNOK","LTCRUB","LTCSEK","LTCTRY","LTCZAR","LTCHKD","LTCCNH","LTCDKK","LTCMXN","LTCPLN","LTCXAG","LTCXAU","LTCETH","LTCXRP","LTCDSH","LTCBCH","XRPEUR","XRPJPY","XRPGBP","XRPCAD","XRPCHF","XRPAUD","XRPNZD","XRPSGD","XRPNOK","XRPRUB","XRPSEK","XRPTRY","XRPZAR","XRPHKD","XRPCNH","XRPDKK","XRPMXN","XRPPLN","XRPXAG","XRPXAU","XRPETH","XRPLTC","XRPDSH","XRPBCH","DSHEUR","DSHJPY","DSHGBP","DSHCAD","DSHCHF","DSHAUD","DSHNZD","DSHSGD","DSHNOK","DSHRUB","DSHSEK","DSHTRY","DSHZAR","DSHHKD","DSHCNH","DSHDKK","DSHMXN","DSHPLN","DSHXAG","DSHXAU","DSHETH","DSHLTC","DSHXRP","DSHBCH","BCHEUR","BCHJPY","BCHGBP","BCHCAD","BCHCHF","BCHAUD","BCHNZD","BCHSGD","BCHNOK","BCHRUB","BCHSEK","BCHTRY","BCHZAR","BCHHKD","BCHCNH","BCHDKK","BCHMXN","BCHPLN","BCHXAG","BCHXAU","BCHETH","BCHLTC","BCHXRP","BCHDSH"&api_key=` + API_KEY
const endpoint = `quotes?pairs=EURUSD,USDJPY,GBPUSD,USDCAD,USDCHF,AUDUSD,NZDUSD,EURGBP,EURCHF,EURCAD,EURAUD,EURNZD,EURJPY,GBPJPY,CHFJPY,CADJPY,AUDJPY,NZDJPY,GBPCHF,GBPAUD,GBPCAD,GBPNZD,AUDCAD,AUDCHF,AUDNZD,CADCHF,AUDSGD,CHFSGD,EURNOK,EURRUB,EURSEK,EURSGD,EURTRY,EURZAR,GBPHKD,GBPNOK,GBPSEK,GBPSGD,NOKJPY,NZDCAD,NZDCHF,SGDJPY,USDCNH,USDDKK,USDHKD,USDMXN&api_key=` + API_KEY
const apiUrl = apiBaseUrl + endpoint

// =========== MAIL CONFIG ===============
let selfSignedConfig = {
  host: 'smtp.gmail.com',
  port: 465,
  secure: true, // use TLS
  auth: {
    user: 'snp100.web@gmail.com',
    pass: 'insert_password_here'
  },
  tls: {
    rejectUnauthorized: false
  }
}
const smtpTransport = mailer.createTransport(selfSignedConfig)

app.use(bodyParser())
app.use(express.json())
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', '*')
  res.header('Access-Control-Allow-Headers', 'origin, content-type, accept')
  next()
})

app.use(express.static(path.join(__dirname, 'build')))

setInterval(function () {
  getFromApi()
}, 120000)

function getFromApi () {
  req.get(apiUrl, function (error, response, body) {
    if (!error && response && response.statusCode === 200) {
      let pairs = JSON.parse(body)
      let time = new Date().toLocaleTimeString()
      let currentPair = {'time': time, 'pairs': pairs}

      saveToDb(currentPair)
    }
  })
}

router.get('/getPairs', async (req, res) => {
  let client
  try {
    client = await MongoClient.connect(dbRoute + dbName, {useNewUrlParser: true})
    const db = client.db(dbName)
    const collection = db.collection('pairs')
    let dbResult = collection
      .find({}, {'_id': 0}).sort({$natural: -1})
      .limit(2)
    dbResult.toArray(function (err, results) {
      if (err) return console.log(err)
      if (results[0] != null && results[1] != null) {
        const pairsNew = results[0].pairs || []
        const pairsOld = results[1].pairs || []
        pairsNew.forEach(function (item, i) {
          item.delta = item.price / pairsOld[i].price * 100 - 100
          item.deltaAbs = (item.price - pairsOld[i].price).toFixed(5)
          if (item.price === pairsOld[i].price) {
            item.delta = 0
          }
          item.delta = item.delta.toFixed(5) + '%'
        })
        res.setHeader('Content-Type', 'application/json')
        res.send(JSON.stringify(pairsNew))
      }
    })
    console.log('got quotes with delta!')
  } catch (err) {
    console.log(err.stack)
  }

  if (client) {
    client.close()
  }
})

// ============= GET CALL REQUEST AND SEND EMAIL ================
router.post('/callRequest', (req, res) => {
  const callReq = req.body
  res.setHeader('Content-Type', 'application/json')
  if (!req.body) return res.sendStatus(400)
  if (callReq.date && callReq.time && callReq.name) {
    let callRequestString = 'Дата: ' + callReq.date + ', ' + callReq.time +
      ', клиент: ' + callReq.name + ', телефон: ' + callReq.phone +
      ' Сообщение: ' + callReq.speech + ' Отправлено ' + callReq.sendingTime

    let mail = {
      from: 'Call Request mailer <snp100.web@gmail.com>',
      to: 'chat@snp100.com',
      subject: 'Call Request',
      text: callRequestString,
      html: '<b>' + callRequestString + '</b>'
    }

    smtpTransport.sendMail(mail, function (error, response) {
      if (error) {
        console.log(response)
        console.log(error)
      }
      smtpTransport.close()
    })

    res.send(JSON.stringify({'sending': callRequestString}))
  }
})

app.use('/api', router)

app.get('/*', function (req, res) {
  res.setHeader('Content-Type', 'text/html')
  res.header('Content-Type', 'text/html')
  res.header('Accept-Ranges', 'bytes')
  res.setHeader('connection', 'keep-alive')
  res.sendFile(path.join(__dirname, 'build', 'index.html'))
})

const httpsServer = https.createServer(credentials, app)
httpsServer.listen(443, () => {
  console.log('HTTPS Server running on port 443')
})

http.createServer(function (req, res) {
  res.writeHead(301, { 'Location': 'https://' + req.headers['host'] + req.url })
  res.end()
}).listen(80)
