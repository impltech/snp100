const MongoClient = require('mongodb').MongoClient
const dbRoute = 'mongodb://localhost:27017/'
const dbName = 'snp100'

module.exports = async function saveToDB (currentPair) {
  let client

  try {
    client = await MongoClient.connect(dbRoute + dbName, {useNewUrlParser: true})
    const db = client.db(dbName)
    const collection = db.collection('pairs')
    collection.insertOne(currentPair)
    console.log('inserting one pairs collection')
  } catch (err) {
    console.log(err.stack)
  }

  if (client) {
    client.close()
  }
}