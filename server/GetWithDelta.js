const MongoClient = require('mongodb').MongoClient
const dbRoute = 'mongodb://localhost:27017/'
const dbName = 'snp100'

module.exports = async function getWithDelta () {
  let client
  try {
    client = await MongoClient.connect(dbRoute + dbName, {useNewUrlParser: true})
    const db = client.db(dbName)
    const collection = db.collection('pairs')
    let dbResult = collection
      .find({}, {'_id': 0}).sort({$natural: -1})
      .limit(2)
    let res = dbResult.toArray(function (err, results) {
      if (err) return console.log(err)
      if (results[0] != null && results[1] != null) {
        const pairsNew = results[0].pairs || []
        const pairsOld = results[1].pairs || []
        pairsNew.forEach(function (item, i) {
          item.delta = item.price / pairsOld[i].price * 100 - 100
          item.deltaAbs = (item.price - pairsOld[i].price).toFixed(5)
          if (item.price === pairsOld[i].price) {
            item.delta = 0
          }
          item.delta = item.delta.toFixed(5) + '%'
        })
        console.log(pairsNew)
        return pairsNew
      }
    })

    console.log('got quotes with delta!')
  } catch (err) {
    console.log(err.stack)
  }

  if (client) {
    client.close()
  }
}
