#!/bin/bash
if [ -e ~/data/db ]
then mongod --dbpath ~/data/db
else mkdir ~/data/db && mongod --dbpath ~/data/db
fi