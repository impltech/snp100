// react
import React, { Component } from 'react'
import Loadable from 'react-loadable'

// components
import Header from '../../components/Header/Header'
import Footer from '../../components/Footer/Footer'
import Sidebar from '../../components/SideBar/SideBar'
import LanguageComponent from '../../components/Modal/LanguageComponent/LanguageComponent'

// styles
import './Layout.css'
import LoadingComponent from '../../components/LoadingComponent/LoadingComponent'

const Chat = Loadable({
  loader: () => import('../../components/Chat/ChatBox'),
  loading: LoadingComponent
})

const Modal = Loadable({
  loader: () => import('../../components/Modal/Modal'),
  loading: LoadingComponent
})

class Layout extends Component {
  state = {
    isHidden: false,
    isOpen: false,
    isModalOpen: false,
    isRotated: false
  }

  showSideBar = () => {
    this.setState({ isHidden: !this.state.isHidden })
  }

  hideSideBar = () => {
    if (this.state.isHidden) {
      this.setState({
        isHidden: !this.state.isHidden,
        isRotated: !this.state.isRotated
      })
    }
  }

  showModal = () => {
    this.setState({
      isModalOpen: true
    })
  }

  closeModal = () => {
    this.setState({
      isModalOpen: false
    })
  }

  menuButtonIsClicked = () => {
    this.setState({
      isRotated: !this.state.isRotated
    })
  }

  render () {
    return (
      <div className="layout">
        <Header
          show={this.showSideBar}
          hide={this.hideSideBar}
          modal_open={this.showModal}
          menuButtonIsClicked={this.menuButtonIsClicked}
          isRotated={this.state.isRotated}
        />
        <div className={'content ' + (this.props.container || 'inner-page')} onClick={this.hideSideBar}>
          <Modal modal_closed={this.closeModal} open={this.state.isModalOpen}>
            <LanguageComponent modal_closed={this.closeModal} />
          </Modal>
          {this.props.children}
        </div>
        <Sidebar hidden={this.state.isHidden} modal_open={this.showModal} />
        <Chat/>
        <Footer modal_open={this.showModal}/>
      </div>
    )
  }
}

export default Layout
