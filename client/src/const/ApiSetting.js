const api = '/api'

export const pairsGet = api + '/getPairs'
export const callRequest = api + '/callRequest'
export const cabinet = 'https://tradersroom.snp100.com/signin'
export const webTrader = 'http://webtrader.snp100.com'
export const registration = 'https://tradersroom.snp100.com/signup'