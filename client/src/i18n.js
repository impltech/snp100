import i18n from 'i18next'
import {reactI18nextModule} from 'react-i18next'
import backend from 'i18next-xhr-backend'

i18n
  .use(backend)
  .use(reactI18nextModule)
  .init({
    // resources,
    lng: 'ru',
    fallbackLng: 'ru',
    // keySeparator: false,

    interpolation: {
      escapeValue: false
    },
    react: {
      wait: true
    }
  })

export default i18n
