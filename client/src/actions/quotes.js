// babel
import 'babel-polyfill'

// redux
import {createActions} from 'redux-actions'

// api
import api from '../api/quotes'

// mappers
import quotes from '../mappers/quotes'
import {SET_STOCKS_ERROR, SET_STOCKS_QUOTES} from './actionsTypes'
import mapStockQuotes from '../api/stockQuotes'

const actions = createActions({
  quotes: {
    request: x => x,
    success: x => x,
    error: x => x
  }
})

export default actions

export const getQuotes = () => async (dispatch, getState) => {
  dispatch(actions.quotes.request())

  try {
    const result = await api.getQuotes()
    const items = result.map(quotes)

    dispatch(
      actions.quotes.success({
        items: items
      })
    )
  } catch (e) {
    dispatch(actions.quotes.error({error: e}))
    console.log(e)
  }
}

export const getStockQuotes = () => async (dispatch) => {
  const stockBasesUrl = 'https://api.iextrading.com/1.0'
  const stockQuery = '/stock/market/batch?symbols=zxiet,zyme,zyne,btcusdt,eosusdt,ethusdt,bnbusdt,ontusdt,bccusdt,' +
    'adausdt,xrpusdt,tusdusdt,trxusdt,ltcusdt,etcusdt,iotausdt,icxusdt,neousdt,venusdt,xlmusdt,qtumusdt,' +
    'aapl,fb,tsla,googl,gm,hp,kfy,kf,jmin,len,lll,med,ndaq,nmrd,pebo,' +
    'pfnx,plus,prpl,anip,anss,aos,apei,apdn,apog,apd,apy,aq,aqua,arc,arcc,artw,arvn,asix,asys,axo,axp,axs,box,' +
    'bpmc,brn,bset,dht,div,dla,dpw,dxc,efx,enob,epam,fc,fds,fdx,ffm,five,flrf,ghc,glad,grif,hmy,hpo,idti,schm' +
    'sga,sgyp,snd,snhy&types=quote&displayPercent=true'
  const stockUrl = stockBasesUrl + stockQuery
  const headers = {
    'Content-Type': 'application/json'
  }
  let responseStatus = false
  try {
    fetch(stockUrl, {
      method: 'GET',
      headers: headers
    })
      .then(response => {
        responseStatus = response.ok
        console.log(responseStatus)
        return response
      })
      .then(response => response.json())
      .then(json => {
        if (responseStatus) {
          console.log(json)
          dispatch(setStockQuotes(mapStockQuotes(json)))
        } else {
          dispatch(setStocksError(json))
        }
      }
      )
  } catch (e) {
    dispatch(setStocksError(e))
    console.log(e)
  }
}

export function setStockQuotes (stocksQuotes) {
  return {type: SET_STOCKS_QUOTES, payload: stocksQuotes}
}

export function setStocksError (errData) {
  return {type: SET_STOCKS_ERROR, payload: errData}
}