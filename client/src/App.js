// react
import React, {Component} from 'react'
import Loadable from 'react-loadable'

import {Switch, BrowserRouter as Router, Route} from 'react-router-dom'
// title info

import DocumentTitle from 'react-document-title'
// components
import Container from './components/Container/Container'
import Bonus from './components/RouteComponents/ForUpperNav/Bonus/Bonus'
import SaleInfo from './components/RouteComponents/ForUpperNav/SaleInfo/SaleInfo'
import NoMatch from './components/NoMatch/NoMatch'
import FAQ from './components/RouteComponents/ForUpperNav/FAQ/FAQ'
import Calendar from './components/RouteComponents/ForMainNav/Calendar/Calendar'
import Debentures from './components/RouteComponents/ForMainNav/Debentures/Debentures'
import Contacts from './components/RouteComponents/ForUpperNav/Contacts/Contacts'
import Stock from './components/RouteComponents/ForFooter/Stock/Stock'
import Indexes from './components/RouteComponents/ForFooter/Indexes/Indexes'
import Products from './components/RouteComponents/ForFooter/Products/Products'
import CFD from './components/RouteComponents/ForFooter/CFD/CFD'
import ScrollToTop from './components/ScrollToTop'
import Forex from './components/RouteComponents/ForFooter/Forex/Forex'
import CryptoCurrency from './components/RouteComponents/ForMainNav/CryptoCurrency/CryptoCurrency'
import Investment from './components/RouteComponents/ForMainNav/Investment/Investment'
import AccountsTypes from './components/RouteComponents/ForUpperNav/AccountsTypes/AccountsTypes'
import LoadingComponent from './components/LoadingComponent/LoadingComponent'

const Risk = Loadable({
  loader: () => import('./components/RouteComponents/ForFooter/PDFDocs/Risk'),
  loading: LoadingComponent
})

const TermsOfUse = Loadable({
  loader: () => import('./components/RouteComponents/ForFooter/PDFDocs/TermsOfUse'),
  loading: LoadingComponent
})

const LawInfo = Loadable({
  loader: () => import('./components/RouteComponents/ForFooter/PDFDocs/LawInfo'),
  loading: LoadingComponent
})

class App extends Component {
  render () {
    return (
      <DocumentTitle title="S&P100">
        <div>
          <Router>
            <ScrollToTop>
              <div>
                <Switch>
                  <Route exact path="/" component={Container}/>
                  <Route path="/bonus" component={Bonus}/>
                  <Route path="/saleinfo" component={SaleInfo}/>
                  <Route path="/faq" component={FAQ}/>
                  <Route path="/calendar" component={Calendar}/>
                  <Route path="/debentures" component={Debentures}/>
                  <Route path="/contacts" component={Contacts}/>
                  <Route path="/stock" component={Stock}/>
                  <Route path="/forex" component={Forex}/>
                  <Route path="/indexes" component={Indexes}/>
                  <Route path="/products" component={Products}/>
                  <Route path="/cfd" component={CFD}/>
                  <Route path="/crypto" component={CryptoCurrency}/>
                  <Route path="/investment" component={Investment}/>
                  <Route path="/risk" component={Risk}/>
                  <Route path="/lawinfo" component={LawInfo}/>
                  <Route path="/terms" component={TermsOfUse}/>
                  <Route path="/accounts" component={AccountsTypes}/>
                  <Route component={NoMatch}/>
                </Switch>
              </div>
            </ScrollToTop>
          </Router>
        </div>
      </DocumentTitle>
    )
  }
}

export default App
