import {pairsGet} from '../const/ApiSetting'

export const API_KEY = 'ONSW5JROgcFmgb2facc5OqoCHhpMWtvN'

const apiBaseUrl = 'https://forex.1forge.com/1.0.3a'

const apiUrl = endpoint => apiBaseUrl + endpoint

const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'

}

export const get = endpoint =>
  fetch(apiUrl(endpoint), {
    method: 'GET',
    headers: headers
  }).then(response => response.json())

export const getsrv = () =>
  fetch(pairsGet, {
    method: 'GET',
    headers: headers
  }).then(response => response.json())
