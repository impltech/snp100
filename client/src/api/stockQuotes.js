function mapStockQuotes (stocks) {
  let stocksArray = []
  for (let key in stocks) {
    stocksArray.push(stocks[key].quote)
  }
  return stocksArray
}

export default mapStockQuotes