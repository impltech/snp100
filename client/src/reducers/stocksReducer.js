import {SET_STOCKS_ERROR, SET_STOCKS_QUOTES} from '../actions/actionsTypes'

const initialState = {
  stocks: [],
  stocksError: null
}

function stocksReducer (state = initialState, action) {
  switch (action.type) {
    case (SET_STOCKS_QUOTES): {
      return {...state, stocks: action.payload}
    }
    case (SET_STOCKS_ERROR): {
      return {...state, stocksError: action.payload}
    }
    default: {
      return state
    }
  }
}

export default stocksReducer