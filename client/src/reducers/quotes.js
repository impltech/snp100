// Redux
import { handleActions } from 'redux-actions'

// Actions
import actions from '../actions/quotes'

export const initialState = {
  isFetching: false,
  error: '',
  items: []
}

export default handleActions(
  {
    [actions.quotes.request]: state => ({
      ...state,
      isFetching: true,
      error: '',
    }),

    [actions.quotes.success]: (state, { payload }) => ({
      ...state,
      isFetching: false,
      items: payload.items,
    }),

    [actions.quotes.error]: (state, { payload }) => ({
      ...state,
      isFetching: false,
      error: payload.error,
    }),
  },
  initialState,
)
