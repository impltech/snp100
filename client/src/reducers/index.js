// redux
import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

// reducers
import quotes from './quotes'
import stocksReducer from './stocksReducer'

export default combineReducers({
  routing: routerReducer,
  quotes,
  stocksReducer
})
