// react
import React from 'react'
import {t} from 'i18next'

// styles
import '../../FAQ.css'
import {withNamespaces} from 'react-i18next'
import {emails} from '../../../Contacts/emails'

const Deactivation = () => {
  return (
    <div className="faq-answers-block">
      <h3 className='question'>{t('FAQ-account-deactivation-q-1')}</h3>
      <p className='answer-p'>{t('FAQ-account-deactivation-a-1')}{' '}<a href={'mailto:' + emails.support}>{t('FAQ-account-deactivation-a-1-link')}</a></p>
    </div>
  )
}

export default withNamespaces()(Deactivation)
