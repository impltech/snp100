// react
import React, {Component} from 'react'
import {t} from 'i18next'

// styles
import '../../FAQ.css'
import {withNamespaces} from 'react-i18next'
import {dropdown, setRollUp} from '../../../../../Dropdown/Dropdown'

class Contributions extends Component {
  componentDidMount () {
    setRollUp()
  }

  render () {
    return (
      <div className="faq-answers-block">
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-contributions-q-1')}</h3>
          <p>{t('FAQ-contributions-a-1')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-contributions-q-2')}</h3>
          <p>{t('FAQ-contributions-a-2')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-contributions-q-3')}</h3>
          <p>{t('FAQ-contributions-a-3')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-contributions-q-4')}</h3>
          <p>{t('FAQ-contributions-a-4')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-contributions-q-5')}</h3>
          <p>{t('FAQ-contributions-a-5')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-contributions-q-6')}</h3>
          <p>{t('FAQ-contributions-a-6')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-contributions-q-7')}</h3>
          <p>{t('FAQ-contributions-a-7')}</p>
        </div>
      </div>
    )
  }
}

export default withNamespaces()(Contributions)
