// react
import React,{Component} from 'react'
import {t} from 'i18next'

// styles
import '../../FAQ.css'
import {withNamespaces} from 'react-i18next'
import {dropdown, setRollUp} from '../../../../../Dropdown/Dropdown'

class Registration extends Component {
  componentDidMount () {
    setRollUp()
  }

  render () {
    return (
      <div className="faq-answers-block">
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-1')}</h3>
          <p>{t('FAQ-registration-a-1')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-2')}</h3>
          <p>{t('FAQ-registration-a-2')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-3')}</h3>
          <p>{t('FAQ-registration-a-3')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-4')}</h3>
          <p>{t('FAQ-registration-a-4.p-1')}</p>
          <p>{t('FAQ-registration-a-4.p-2')}</p>
          <p>{t('FAQ-registration-a-4.p-3')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-5')}</h3>
          <p>{t('FAQ-registration-a-5')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-6')}</h3>
          <p>{t('FAQ-registration-a-6')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-7')}</h3>
          <p>{t('FAQ-registration-a-7.p-1')}</p>
          <p>{t('FAQ-registration-a-7.p-2')}</p>
          <p>{t('FAQ-registration-a-7.p-3')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-8')}</h3>
          <p>{t('FAQ-registration-a-8.p-1')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-9')}</h3>
          <p>{t('FAQ-registration-a-9')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-10')}</h3>
          <p>{t('FAQ-registration-a-10')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-11')}</h3>
          <p>{t('FAQ-registration-a-11')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-12')}</h3>
          <p>{t('FAQ-registration-a-12')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-13')}</h3>
          <p>{t('FAQ-registration-a-13')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-14')}</h3>
          <p>{t('FAQ-registration-a-14')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-15')}</h3>
          <p>{t('FAQ-registration-a-15')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-16')}</h3>
          <p>{t('FAQ-registration-a-16')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-17')}</h3>
          <p>{t('FAQ-registration-a-17')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-18')}</h3>
          <p>{t('FAQ-registration-a-18')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-registration-q-19')}</h3>
          <p>{t('FAQ-registration-a-19')}</p>
        </div>

      </div>
    )
  }
}

export default withNamespaces()(Registration)
