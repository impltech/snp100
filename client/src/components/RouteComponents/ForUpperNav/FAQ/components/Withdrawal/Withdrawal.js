// react
import React, {Component} from 'react'
import {t} from 'i18next'

// styles
import '../../FAQ.css'
import {withNamespaces} from 'react-i18next'
import Link from 'react-router-dom/es/Link'
import {dropdown, setRollUp} from '../../../../../Dropdown/Dropdown'
import {emails} from '../../../Contacts/emails'
import {cabinet} from '../../../../../../const/ApiSetting'

class Withdrawal extends Component {
  componentDidMount () {
    setRollUp()
  }

  render () {
    return (
      <div className="faq-answers-block">
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-cash-withdrawal-q-1')}</h3>
          <p>{t('FAQ-cash-withdrawal-a-1-span-1.p-1')}</p>
          <p>{t('FAQ-cash-withdrawal-a-1-span-1.p-2')}</p>
          <p>{t('FAQ-cash-withdrawal-a-1-span-1.p-3')}</p>
          <p>{t('FAQ-cash-withdrawal-a-1-span-1.p-4')}{' '}
            <a href={'mailto:' + emails.withdrawal}>{emails.withdrawal}</a>
            {' '}{t('FAQ-cash-withdrawal-a-1-span-2')}
          </p>
        </div>

        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-cash-withdrawal-q-2')}</h3>
          <p>{t('FAQ-cash-withdrawal-a-2')}</p>
        </div>

        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-cash-withdrawal-q-3')}</h3>
          <p>{t('FAQ-cash-withdrawal-a-3')}</p>
          <ol>
            <li>{t('FAQ-cash-withdrawal-ol-li-1')}{' '}
              <Link to='/faq/verification'>{t('FAQ-cash-withdrawal-ol-li-1-link')}</Link>
            </li>
            <li>{t('FAQ-cash-withdrawal-ol-li-2')}</li>
            <li>{t('FAQ-cash-withdrawal-ol-li-3')}</li>
          </ol>
        </div>

        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-cash-withdrawal-q-4')}</h3>
          <p>{t('FAQ-cash-withdrawal-a-4.p-1')}</p>
          <p>{t('FAQ-cash-withdrawal-a-4.p-2')}</p>
        </div>

        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-cash-withdrawal-q-5')}</h3>
          <p>{t('FAQ-cash-withdrawal-a-5.p-1')}</p>
          <p>{t('FAQ-cash-withdrawal-a-5.p-2')}</p>
        </div>

        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-cash-withdrawal-q-6')}</h3>
          <p>{t('FAQ-cash-withdrawal-a-6.p-1')}</p>
          <p>{t('FAQ-cash-withdrawal-a-6.p-2')}</p>
        </div>

        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-cash-withdrawal-q-7')}</h3>
          <p>{t('FAQ-cash-withdrawal-a-7')}</p>
        </div>

        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-cash-withdrawal-q-8')}</h3>
          <p>{t('FAQ-cash-withdrawal-a-8-span-1')}{' '}
            <a href={cabinet}>{t('FAQ-cash-withdrawal-a-8-link')}</a>
            {' '}{t('FAQ-cash-withdrawal-a-8-span-2')}</p>
        </div>

        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-cash-withdrawal-q-9')}</h3>
          <p>{t('FAQ-cash-withdrawal-a-9.p-1')}</p>
          <p>{t('FAQ-cash-withdrawal-a-9.p-2')}</p>
          <p>{t('FAQ-cash-withdrawal-a-9.p-3')}</p>
          <p>{t('FAQ-cash-withdrawal-a-9.p-4')}</p>
        </div>

        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-cash-withdrawal-q-10')}</h3>
          <p>{t('FAQ-cash-withdrawal-a-10.p-1')}</p>
          <p>{t('FAQ-cash-withdrawal-a-10.p-2')}</p>
        </div>
      </div>
    )
  }
}

export default withNamespaces()(Withdrawal)
