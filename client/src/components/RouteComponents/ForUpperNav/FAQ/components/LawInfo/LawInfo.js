// react
import React from 'react'
import {t} from 'i18next'

// styles
import '../../FAQ.css'
import {withNamespaces} from 'react-i18next'

const LawInfo = () => {
  return (
    <div className="faq-answers-block">
      <h3 className='question'>{t('FAQ-law-info-q-1')}</h3>
      <p className='answer-p'>
        {t('FAQ-law-info-a-1')}
      </p>
    </div>
  )
}

export default withNamespaces()(LawInfo)
