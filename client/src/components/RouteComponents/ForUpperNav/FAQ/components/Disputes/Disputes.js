// react
import React from 'react'
import {t} from 'i18next'

// styles
import '../../FAQ.css'
import {withNamespaces} from 'react-i18next'
import {emails} from '../../../Contacts/emails'

const Disputes = () => {
  return (
    <div className="faq-answers-block">
      <h3 className='answer-header'>{t('FAQ-disputes-h-1')}</h3>
      <p>
        {t('FAQ-disputes-p-1-span-1')} {' '}
        <a href={'mailto://' + emails.support}>{emails.support}</a>
        {' '}{t('FAQ-disputes-p-1-span-2')}
      </p>
      <h3 className='answer-header'>{t('FAQ-disputes-h-2')}</h3>
      <p>{t('FAQ-disputes-p-2-span-1.p-1')}</p>
      <p>
        {t('FAQ-disputes-p-2-span-1.p-2')} {' '}
        <a href={'mailto://' + emails.compliance}>{emails.compliance}</a>{' '}
        {t('FAQ-disputes-p-2-span-2')}
      </p>
    </div>
  )
}

export default withNamespaces()(Disputes)
