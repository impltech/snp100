// react
import React, {Component} from 'react'
import {t} from 'i18next'

// styles
import '../../FAQ.css'
import {withNamespaces} from 'react-i18next'
import {dropdown, setRollUp} from '../../../../../Dropdown/Dropdown'
import {emails} from '../../../Contacts/emails'

class Verify extends Component {
  componentDidMount () {
    setRollUp()
  }

  render () {
    return (
      <div className="faq-answers-block">
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-verify-q-1')}</h3>
          <p>{t('FAQ-verify-a-1.p-1')}</p>
          <p>{t('FAQ-verify-a-1.p-2')}</p>
        </div>

        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-verify-q-2')}</h3>
          <p>{t('FAQ-verify-a-2-1')}{' '}
            <a href={'mailto://' + emails.billing}>{emails.billing}</a>
            {' '}{t('FAQ-verify-a-2-2')}
          </p>
        </div>

        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-verify-q-3')}</h3>
          <p>{t('FAQ-verify-a-3')}</p>
        </div>

        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-verify-q-4')}</h3>
          <p>{t('FAQ-verify-a-4')}</p>
        </div>

        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-verify-q-5')}</h3>
          <p>{t('FAQ-verify-a-5')}</p>
        </div>

        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-verify-q-6')}</h3>
          <p>{t('FAQ-verify-a-6.p-1')}</p>
          <p>{t('FAQ-verify-a-6.p-2')}</p>
        </div>
      </div>
    )
  }
}

export default withNamespaces()(Verify)
