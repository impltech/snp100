// react
import React, {Component} from 'react'
import {t} from 'i18next'

// styles
import '../../FAQ.css'
import {withNamespaces} from 'react-i18next'
import {dropdown, setRollUp} from '../../../../../Dropdown/Dropdown'

class TradeInfo extends Component {
  componentDidMount () {
    setRollUp()
  }

  render () {
    return (
      <div className="faq-answers-block">
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-1')}</h3>
          <p>{t('FAQ-trade-information-a-1')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-2')}</h3>
          <p>{t('FAQ-trade-information-a-2')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-3')}</h3>
          <p>{t('FAQ-trade-information-a-3')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-4')}</h3>
          <p>{t('FAQ-trade-information-a-4.p-1')}</p>
          <p>{t('FAQ-trade-information-a-4.p-2')}</p>
          <p>{t('FAQ-trade-information-a-4.p-3')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-5')}</h3>
          <p>{t('FAQ-trade-information-a-5')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-6')}</h3>
          <p>{t('FAQ-trade-information-a-6')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-7')}</h3>
          <p>{t('FAQ-trade-information-a-7')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-8')}</h3>
          <p>{t('FAQ-trade-information-a-8')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-9')}</h3>
          <p>{t('FAQ-trade-information-a-9')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-10')}</h3>
          <p>{t('FAQ-trade-information-a-10.p-1')}</p>
          <p>{t('FAQ-trade-information-a-10.p-2')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-11')}</h3>
          <p>{t('FAQ-trade-information-a-11')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-12')}</h3>
          <p>{t('FAQ-trade-information-a-12')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-13')}</h3>
          <p>{t('FAQ-trade-information-a-13.p-1')}</p>
          <p>{t('FAQ-trade-information-a-13.p-2')}</p>
          <ul>
            <li>{t('FAQ-trade-information-a-13-li-1')}</li>
            <li>{t('FAQ-trade-information-a-13-li-2')}</li>
          </ul>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-14')}</h3>
          <p>{t('FAQ-trade-information-a-14')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-15')}</h3>
          <p>{t('FAQ-trade-information-a-15')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-16')}</h3>
          <p>{t('FAQ-trade-information-a-16.p-1')}</p>
          <p>{t('FAQ-trade-information-a-16.p-2')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-17')}</h3>
          <p>{t('FAQ-trade-information-a-17')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-18')}</h3>
          <p>{t('FAQ-trade-information-a-18.p-1')}</p>
          <p>{t('FAQ-trade-information-a-18.p-2')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-19')}</h3>
          <p>{t('FAQ-trade-information-a-19')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-20')}</h3>
          <p>{t('FAQ-trade-information-a-20')}</p>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-21')}</h3>
          <p>{t('FAQ-trade-information-a-21')}</p>
          <ul>
            <li>{t('FAQ-trade-information-a-21-li-1')}</li>
            <li>{t('FAQ-trade-information-a-21-li-2')}</li>
          </ul>
        </div>
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-information-q-22')}</h3>
          <p>{t('FAQ-trade-information-a-22.p-1')}</p>
          <p>{t('FAQ-trade-information-a-22.p-2')}</p>
        </div>

      </div>
    )
  }
}

export default withNamespaces()(TradeInfo)
