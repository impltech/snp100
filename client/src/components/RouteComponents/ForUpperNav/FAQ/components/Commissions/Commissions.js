// react
import React, {Component} from 'react'
import {t} from 'i18next'

// styles
import '../../FAQ.css'
import {withNamespaces} from 'react-i18next'
import {dropdown, setRollUp} from '../../../../../Dropdown/Dropdown'

class Commissions extends Component {
  componentDidMount () {
    setRollUp()
  }

  render () {
    return (
      <div className="faq-answers-block">
        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-commissions-q-1')}</h3>
          <p>{t('FAQ-trade-commissions-a-1.p-1')}</p>
          <p>{t('FAQ-trade-commissions-a-1.p-2')}</p>
        </div>

        <div className='dropdown-paragraph' onClick={dropdown}>
          <h3>{t('FAQ-trade-commissions-q-2')}</h3>
          <p>{t('FAQ-trade-commissions-a-2')}</p>
        </div>
      </div>
    )
  }
}

export default withNamespaces()(Commissions)
