// react
import React, {Component} from 'react'
import {Switch, Route, Link} from 'react-router-dom'
import DocumentTitle from 'react-document-title'

// translation
import {withNamespaces} from 'react-i18next'
import {t} from 'i18next'

// hocs
import Layout from '../../../../hocs/Layout/Layout'

// styles
import './FAQ.css'
import arrow from '../../../../assets/innerPages/angle-arrow-down.png'

// components
import HelpColumn from '../../HelpColumn/HelpColumn'
import Disputes from './components/Disputes/Disputes'
import Registration from './components/Registration/Registration'
import Commissions from './components/Commissions/Commissions'
import Contributions from './components/Contributions/Contributions'
import Deactivation from './components/Deactivation/Deactivation'
import LawInfo from './components/LawInfo/LawInfo'
import TradeInfo from './components/TradeInfo/TradeInfo'
import Verify from './components/Verify/Verify'
import Withdrawal from './components/Withdrawal/Withdrawal'
import {setRollUp} from '../../../Dropdown/Dropdown'

class FAQ extends Component {
  constructor (props) {
    super(props)
    this.state = {title: 'FAQ'}
  }

  setTitle = () => {
    const currentRoute = this.props.location.pathname
    switch (currentRoute) {
      case '/faq/disputes': {
        return (this.setState({'title': 'FAQ-disputes-H'}))
      }
      case '/faq/registration': {
        return (this.setState({'title': 'FAQ-registration-H'}))
      }
      case '/faq/commissions': {
        return (this.setState({'title': 'FAQ-trade-commissions-H'}))
      }
      case '/faq/contributions': {
        return (this.setState({'title': 'FAQ-contributions-H'}))
      }
      case '/faq/deactivation': {
        return (this.setState({'title': 'FAQ-account-deactivation-H'}))
      }
      case '/faq/law-info': {
        return (this.setState({'title': 'FAQ-law-info-H'}))
      }
      case '/faq/trade-info': {
        return (this.setState({'title': 'FAQ-trade-information-H'}))
      }
      case '/faq/verification': {
        return (this.setState({'title': 'FAQ-verify-H'}))
      }
      case '/faq/withdrawal': {
        return (this.setState({'title': 'FAQ-cash-withdrawal-H'}))
      }
      default: {
        this.setState({'title': 'faq'})
      }
    }
  }

  componentDidMount = () => {
    this.setTitle()
    setRollUp()
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      this.setTitle()
    }
  }

  render () {
    const onPage = this.props.location.pathname
    console.log(onPage)
    return (
      <div>
        <DocumentTitle title={t(this.state.title)}>
          <Layout>
            <div className="faq-block">
              <div className="in-block-nav">
                <Link to="/">{t('main')}</Link>
                <img src={arrow} alt='' className={'arrow-right'}/>
                <Link to="/faq/disputes">FAQ</Link>
                <img src={arrow} alt='' className={'arrow-right'}/>
                {t(this.state.title)}
              </div>

              <h1>
                {t(this.state.title)}
              </h1>

              <section className="faq">
                <div className="left-faq-col">
                  <Link to="/faq/disputes">
                    <span className={(onPage === '/faq/disputes') ? 'active-route' : 'route'}>
                      <span>{t('FAQ-disputes-H')}</span>
                    </span>
                  </Link>
                  <Link to="/faq/registration">
                    <span className={(onPage === '/faq/registration') ? 'active-route' : 'route'}>
                      <span>{t('FAQ-registration-H')}</span>
                    </span>
                  </Link>
                  <Link to="/faq/law-info">
                    <span className={(onPage === '/faq/law-info') ? 'active-route' : 'route'}>
                      <span>{t('FAQ-law-info-H')}</span>
                    </span>
                  </Link>
                  <Link to="/faq/verification">
                    <span className={(onPage === '/faq/verification') ? 'active-route' : 'route'}>
                      <span>{t('FAQ-verify-H')}</span>
                    </span>
                  </Link>
                  <Link to="/faq/contributions">
                    <span className={(onPage === '/faq/contributions') ? 'active-route' : 'route'}>
                      <span>{t('FAQ-contributions-H')}</span>
                    </span>
                  </Link>
                  <Link to="/faq/trade-info">
                    <span className={(onPage === '/faq/trade-info') ? 'active-route' : 'route'}>
                      <span>{t('FAQ-trade-information-H')}</span>
                    </span>
                  </Link>
                  <Link to="/faq/commissions">
                    <span className={(onPage === '/faq/commissions') ? 'active-route' : 'route'}>
                      <span>{t('FAQ-trade-commissions-H')}</span>
                    </span>
                  </Link>
                  <Link to="/faq/withdrawal">
                    <span className={(onPage === '/faq/withdrawal') ? 'active-route' : 'route'}>
                      <span>{t('FAQ-cash-withdrawal-H')}</span>
                    </span>
                  </Link>
                  <Link to="/faq/deactivation">
                    <span className={(onPage === '/faq/deactivation') ? 'active-route' : 'route'}>
                      <span>{t('FAQ-account-deactivation-H')}</span>
                    </span>
                  </Link>
                </div>
                <div className="center-faq-col">
                  <Switch>
                    <Route path="/faq/disputes" component={Disputes}/>
                    <Route path="/faq/registration" component={Registration}/>
                    <Route path="/faq/commissions" component={Commissions}/>
                    <Route path="/faq/contributions" component={Contributions}/>
                    <Route path="/faq/deactivation" component={Deactivation}/>
                    <Route path="/faq/law-info" component={LawInfo}/>
                    <Route path="/faq/trade-info" component={TradeInfo}/>
                    <Route path="/faq/verification" component={Verify}/>
                    <Route path="/faq/withdrawal" component={Withdrawal}/>
                  </Switch>
                </div>
                <HelpColumn/>
              </section>
            </div>
          </Layout>
        </DocumentTitle>
      </div>
    )
  }
}

export default withNamespaces()(FAQ)
