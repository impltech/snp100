// react
import React from 'react'
import { Link } from 'react-router-dom'

// title info
import DocumentTitle from 'react-document-title'

// hocs
import Layout from '../../../../hocs/Layout/Layout'

// styles
import './Bonus.css'

// translation
import {withNamespaces} from 'react-i18next'
import {t} from 'i18next'

// components
import HelpColumn from '../../HelpColumn/HelpColumn'
import arrow from '../../../../assets/innerPages/angle-arrow-down.png'

const Bonus = () => {
  return (
    <Layout>
      <DocumentTitle title={t('bonuses')}>
        <div className="content-block">
          <div className="in-block-nav">
            <Link to="/"> {t('main')}</Link>
            <img src={arrow} alt='' className={'arrow-right'}/>
            {t('bonuses')}
          </div>
          <h1>Бонусы</h1>
          <section >
            <div className="left-bonus-col">
              <div>
                <h3>НОВОГОДНИЙ ПОЕДИНОК ТРЕЙДЕРОВ</h3>
                <p>
                  Приближение новогодних праздников всегда ассоциируется с резким
                  ростом волатильности на финансовых рынках и увеличением доходов
                  от торговли. Именно поэтому наша компания проводит соревнование,
                  в котором могут принять участие все опытные трейдеры. В
                  результате напряженного поединка, три самых активных участника,
                  которые покажут наилучшие результаты, а именно: наибольшую
                  доходность от торговли в процентном соотношении, получат
                  денежные призы:
                </p>

                <ul>
                  <li>За 1 место - 20 000 евро;</li>
                  <li>За 2 место - 15 000 евро;</li>
                  <li>За 3 место - 10 000 евро;</li>
                </ul>
                Кроме того, дополнительный призовой фонд распределяется таким
                образом:
                <ul>
                  <li>4 место - 2000 евро;</li>
                  <li>5 место - 1500 евро;</li>
                  <li>6 место - 1000 евро;</li>
                  <li>7 место - 700 евро;</li>
                  <li>8 место - 500 евро;</li>
                  <li>9 место - 200 евро;</li>
                  <li>10 место - 100 евро. </li>
                </ul>
                Стать участником Новогоднего поединка можно, начав с суммы 1 000
                евро. Рекомендуемый депозит - 10 000 евро.
              </div>
              <hr />
              <div>
                <h3>ПАКЕТ ALL INCLUSIVE</h3>
                <p>
                  Только до конца 2018 года при депозите на сумму 1000 евро Вы
                  получите: Разработку персонального инвестиционного портфеля;
                  Доступ ко всем вебинарам и видео-урокам; Качественную
                  аналитику; Обучающие материалы (книги, видеоуроки и много
                  другое); Умножение Вашего капитала; FTO - страховку капитала.
                </p>
              </div>
            </div>
            <HelpColumn />
          </section>
        </div>
      </DocumentTitle>
    </Layout>
  )
}

export default withNamespaces()(Bonus)
