// react
import React, {Component} from 'react'
import {Switch, Route, Link} from 'react-router-dom'
import DocumentTitle from 'react-document-title'

// hocs
import Layout from '../../../../hocs/Layout/Layout'

// styles
import './SaleInfo.css'
import arrow from '../../../../assets/innerPages/angle-arrow-down.png'

// translation
import {withNamespaces} from 'react-i18next'
import {t} from 'i18next'

// components
import ContractsForDiff from './components/ContractsForDiff/ContractsForDiff'
import SaleWithSnp from './components/SaleWithSnp/SaleWithSnp'
import OpenPositions from './components/OpenPositions/OpenPositions'
import ClosePositions from './components/ClosePositions/ClosePositions'
import Leverage from './components/Leverage/Leverage'
import OrderTypes from './components/OrderTypes/OrderTypes'
import Pricing from './components/Pricing/Pricing'
import TradeComissions from './components/TradeСomissions/TradeСomissions'
import AccountVerification from './components/AccountVerification/AccountVerification'
import PaymentMethod from './components/PaymentMethod/PaymentMethod'
import DepositsInfo from './components/DepositsInfo/DepositsInfo'
import WithdrawalOfMoney from './components/WithdrawalOfMoney/WithdrawalOfMoney'
import Bonuses from './components/Bonuses/Bonuses'
import Credits from './components/Credits/Credits'
import HelpColumn from '../../HelpColumn/HelpColumn'

class SaleInfo extends Component {
  constructor (props) {
    super(props)
    this.state = {title: 'S&P100'}
  }

  setTitle = () => {
    const currentRoute = this.props.location.pathname
    switch (currentRoute) {
      case '/saleinfo/diff_contracts': {
        return (this.setState({'title': 'tradeinfo-cdf'}))
      }
      case '/saleinfo/salewith_snp': {
        return (this.setState({'title': 'tradeinfo-trade-with-C&P100'}))
      }
      case '/saleinfo/open_positions': {
        return (this.setState({'title': 'tradeinfo-open-position'}))
      }
      case '/saleinfo/close_positions': {
        return (this.setState({'title': 'tradeinfo-close-position'}))
      }
      case '/saleinfo/leverage': {
        return (this.setState({'title': 'tradeinfo-margin-trade'}))
      }
      case '/saleinfo/order_types': {
        return (this.setState({'title': 'tradeinfo-order-types'}))
      }
      case '/saleinfo/pricing': {
        return (this.setState({'title': 'tradeinfo-pricing'}))
      }
      case '/saleinfo/trade_comissions': {
        return (this.setState({'title': 'tradeinfo-trade-comissions'}))
      }
      case '/saleinfo/account_verification': {
        return (this.setState({'title': 'tradeinfo-account-verification'}))
      }
      case '/saleinfo/payment_method': {
        return (this.setState({'title': 'tradeinfo-payment-method'}))
      }
      case '/saleinfo/deposits_info': {
        return (this.setState({'title': 'tradeinfo-deposits-info'}))
      }
      case '/saleinfo/withdrawal': {
        return (this.setState({'title': 'tradeinfo-withdrawal'}))
      }
      case '/saleinfo/bonuses': {
        return (this.setState({'title': 'tradeinfo-bonuses'}))
      }
      case '/saleinfo/credits': {
        return (this.setState({'title': 'tradeinfo-credits'}))
      }

      default: {
        this.setState({'title': 'tradeinfo-cdf'})
      }
    }
  }

  componentDidMount () {
    this.setTitle()
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      this.setTitle()
    }
  }

  render () {
    const onPage = this.props.location.pathname
    return (
      <div>
        <Layout>
          <DocumentTitle title={t(this.state.title)}>
            <div className="sale-block">

              <div className="in-block-nav">
                <Link to="/">{t('main')}</Link>
                <img src={arrow} alt='' className={'arrow-right'}/>
                <Link to="/saleinfo/diff_contracts">{t('trading info')}</Link>
                <img src={arrow} alt='' className={'arrow-right'}/>
                {t(this.state.title)}
              </div>

              <h1>
                {t(this.state.title)}
              </h1>

              <div className="sale">
                <div className="left-sale-col">
                  <Link to="/saleinfo/diff_contracts" className={(onPage === '/saleinfo/diff_contracts') ? 'active-route' : 'route'}>
                    <span>{t('tradeinfo-cdf')}</span>
                  </Link>
                  <Link to="/saleinfo/salewith_snp" className={(onPage === '/saleinfo/salewith_snp') ? 'active-route' : 'route'}>
                    <span>{t('tradeinfo-trade-with-C&P100')}</span>
                  </Link>
                  <Link to="/saleinfo/open_positions" className={(onPage === '/saleinfo/open_positions') ? 'active-route' : 'route'}>
                    <span>{t('tradeinfo-open-position')}</span>
                  </Link>
                  <Link
                    to="/saleinfo/close_positions" className={(onPage === '/saleinfo/close_positions') ? 'active-route' : 'route'}
                  >
                    <span>{t('tradeinfo-close-position')}</span>
                  </Link>
                  <Link
                    to="/saleinfo/leverage" className={(onPage === '/saleinfo/leverage') ? 'active-route' : 'route'}
                  >
                    <span>{t('tradeinfo-margin-trade')}</span>
                  </Link>
                  <Link
                    to="/saleinfo/order_types" className={(onPage === '/saleinfo/order_types') ? 'active-route' : 'route'}
                  >
                    <span>{t('tradeinfo-order-types')}</span>
                  </Link>
                  <Link
                    to="/saleinfo/pricing" className={(onPage === '/saleinfo/pricing') ? 'active-route' : 'route'}
                  >
                    <span>{t('tradeinfo-pricing')}</span>
                  </Link>
                  <Link
                    to="/saleinfo/trade_comissions" className={(onPage === '/saleinfo/trade_comissions') ? 'active-route' : 'route'}
                  >
                    <span>{t('tradeinfo-trade-comissions')}</span>
                  </Link>
                  <Link
                    to="/saleinfo/account_verification" className={(onPage === '/saleinfo/account_verification') ? 'active-route' : 'route'}
                  >
                    <span>{t('tradeinfo-account-verification')}</span>
                  </Link>
                  <Link
                    to="/saleinfo/payment_method" className={(onPage === '/saleinfo/payment_method') ? 'active-route' : 'route'}
                  >
                    <span>{t('tradeinfo-payment-method')}</span>
                  </Link>
                  <Link
                    to="/saleinfo/deposits_info" className={(onPage === '/saleinfo/deposits_info') ? 'active-route' : 'route'}
                  >
                    <span>{t('tradeinfo-deposits-info')}</span>
                  </Link>
                  <Link
                    to="/saleinfo/withdrawal" className={(onPage === '/saleinfo/withdrawal') ? 'active-route' : 'route'}
                  >
                    <span>{t('tradeinfo-withdrawal')}</span>
                  </Link>
                  <Link
                    to="/saleinfo/bonuses" className={(onPage === '/saleinfo/bonuses') ? 'active-route' : 'route'}
                  >
                    <span>{t('tradeinfo-bonuses')}</span>
                  </Link>
                  <Link
                    to="/saleinfo/credits" className={(onPage === '/saleinfo/credits') ? 'active-route' : 'route'}
                  >
                    <span>{t('tradeinfo-credits')}</span>
                  </Link>
                </div>
                <div className="center-sale-col">
                  <Switch>
                    <Route
                      exact
                      path="/saleinfo/diff_contracts"
                      component={ContractsForDiff}
                    />
                    <Route
                      path="/saleinfo/salewith_snp"
                      component={SaleWithSnp}
                    />
                    <Route
                      path="/saleinfo/open_positions"
                      component={OpenPositions}
                    />
                    <Route
                      path="/saleinfo/close_positions"
                      component={ClosePositions}
                    />
                    <Route path="/saleinfo/leverage" component={Leverage}/>
                    <Route path="/saleinfo/order_types" component={OrderTypes}/>
                    <Route path="/saleinfo/pricing" component={Pricing}/>
                    <Route
                      path="/saleinfo/trade_comissions"
                      component={TradeComissions}
                    />
                    <Route
                      path="/saleinfo/account_verification"
                      component={AccountVerification}
                    />
                    <Route
                      path="/saleinfo/payment_method"
                      component={PaymentMethod}
                    />
                    <Route
                      path="/saleinfo/deposits_info"
                      component={DepositsInfo}
                    />
                    <Route
                      path="/saleinfo/withdrawal"
                      component={WithdrawalOfMoney}
                    />
                    <Route
                      path="/saleinfo/bonuses"
                      component={Bonuses}
                    />
                    <Route path="/saleinfo/credits" component={Credits}/>
                  </Switch>
                </div>
                <HelpColumn/>
              </div>
            </div>
          </DocumentTitle>
        </Layout>
      </div>
    )
  }
}

export default withNamespaces()(SaleInfo)
