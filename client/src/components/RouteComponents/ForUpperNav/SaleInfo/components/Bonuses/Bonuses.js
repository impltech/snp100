// react
import React from 'react'

import {withNamespaces} from 'react-i18next'
import {t} from 'i18next'

// styles
import './Bonuses.css'

const Bonuses = () => {
  return (
    <div className="bonuses">
      <h3>{t('tradeinfo-bonuses-h')}</h3>
      <ul>
        <li>
          {t('tradeinfo-bonuses-li-1')}
        </li>
        <li>
          {t('tradeinfo-bonuses-li-2')}
        </li>
        <li>
          {t('tradeinfo-bonuses-li-3')}
        </li>
        <li>
          {t('tradeinfo-bonuses-li-4')}
        </li>
        <li>
          {t('tradeinfo-bonuses-li-5')}
        </li>
        <li>
          {t('tradeinfo-bonuses-li-6')}
        </li>
        <li>
          {t('tradeinfo-bonuses-li-7')}
        </li>
        <li>
          {t('tradeinfo-bonuses-li-8')}
        </li>
        <li>
          {t('tradeinfo-bonuses-li-9')}
        </li>
        <li>
          {t('tradeinfo-bonuses-li-10')}
        </li>
        <li>
          {t('tradeinfo-bonuses-li-11')}
        </li>
        <li>
          {t('tradeinfo-bonuses-li-12')}
        </li>
        <li>
          {t('tradeinfo-bonuses-li-13')}
        </li>
      </ul>
    </div>
  )
}

export default withNamespaces()(Bonuses)
