// react
import React from 'react'

// styles
import './AccountVerification.css'
import {withNamespaces} from 'react-i18next'
import {t} from 'i18next'

const AccountVerification = () => {
  return (
    <div className="acc-verification">
      <h2>{t('tradeinfo-account-verification')}</h2>
      <p>
        {t('tradeinfo-account-verification-p-1.p-1')}
      </p>
      <p>{t('tradeinfo-account-verification-p-1.p-2')}</p>
      <p>
        {t('tradeinfo-account-verification-p-1.p-3')}
      </p>
      <ul>
        <li>{t('tradeinfo-account-verification-li-1')}</li>
        <li>{t('tradeinfo-account-verification-li-2')}</li>
        <li>{t('tradeinfo-account-verification-li-3')}</li>
      </ul>
      <p>
        {t('tradeinfo-account-verification-p-2.p-1')}
      </p>
      <p>
        {t('tradeinfo-account-verification-p-2.p-2')}
      </p>
      <p>{t('tradeinfo-account-verification-p-2.p-3')}</p>
      <h3>{t('tradeinfo-account-verification-h-1')}</h3>
      <p>
        {t('tradeinfo-account-verification-pp-1')}
      </p>
      <h3>{t('tradeinfo-account-verification-h-2')}</h3>
      <p>
        {t('tradeinfo-account-verification-pp-2')}
      </p>
      <ul>
        <li>
          {t('tradeinfo-account-verification-li-4')}
        </li>
        <li>{t('tradeinfo-account-verification-li-5')}</li>
        <li>{t('tradeinfo-account-verification-li-6')}</li>
      </ul>
      <p>
        {t('tradeinfo-account-verification-pp-3')}
      </p>
    </div>
  )
}

export default withNamespaces()(AccountVerification)
