// react
import React from 'react'

import {withNamespaces} from 'react-i18next'
import {t} from 'i18next'

// styles
import './DepositsInfo.css'

const DepositsInfo = () => {
  return (
    <div className="deposits">
      <h3>{t('tradeinfo-deposits-info-h-1')}</h3>
      <p>
        {t('tradeinfo-deposits-info-p-1')}
      </p>
      <h3>{t('tradeinfo-deposits-info-h-2')}</h3>
      <p>
        {t('tradeinfo-deposits-info-p-2.p-1')}
      </p>
      <p>
        {t('tradeinfo-deposits-info-p-2.p-2')}
      </p>
      <h3>{t('tradeinfo-deposits-info-h-3')}</h3>
      <ol>
        <li>
          {t('tradeinfo-deposits-info-ol-li-1')}
        </li>
        <li>
          {t('tradeinfo-deposits-info-ol-li-2')}
        </li>
        <li>
          {t('tradeinfo-deposits-info-ol-li-3')}
        </li>
      </ol>
      <p>
        {t('tradeinfo-deposits-info-p-3')}
      </p>
      <h3>{t('tradeinfo-deposits-info-h-4')}</h3>
      <ol>
        <li>
          {t('tradeinfo-deposits-info-ol-li-4')}
        </li>
        <li>{t('tradeinfo-deposits-info-ol-li-5')}</li>
        <li>
          {t('tradeinfo-deposits-info-ol-li-6')}
        </li>
      </ol>
      <p>
        {t('tradeinfo-deposits-info-p-4')}
      </p>
      <h3>{t('tradeinfo-deposits-info-h-5')}</h3>
      <p>
        {t('tradeinfo-deposits-info-p-5.p-1')}
      </p>
      <p>
        {t('tradeinfo-deposits-info-p-5.p-2')}
      </p>
      <p>
        {t('tradeinfo-deposits-info-p-5.p-3')}
      </p>
      <p>
        {t('tradeinfo-deposits-info-p-5.p-4')}
      </p>
    </div>
  )
}

export default withNamespaces()(DepositsInfo)
