// react
import React from 'react'

// styles
import './ClosePositions.css'

import {withNamespaces} from 'react-i18next'
import {t} from 'i18next'

const ClosePositions = () => {
  return (
    <div className="open-positions">
      <p>
        {t('tradeinfo-close-position-p')}
      </p>
    </div>
  )
}

export default withNamespaces()(ClosePositions)
