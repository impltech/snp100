// react
import React from 'react'

// styles
import './TradeСomissions.css'

import {t} from 'i18next'
import {withNamespaces} from 'react-i18next'

const TradeComissions = () => {
  return (
    <div className="trade-comission">
      <h3>{t('tradeinfo-trade-comissions-h-1')}</h3>
      <p>
        {t('tradeinfo-trade-comissions-p-1')}
      </p>
      <h3>{t('tradeinfo-trade-comissions-h-2')}</h3>
      <p>
        {t('tradeinfo-trade-comissions-p-2.p-1')}
      </p>
      <p>
        {t('tradeinfo-trade-comissions-p-2.p-2')}
      </p>
      <h3>{t('tradeinfo-trade-comissions-h-3')}</h3>
      <p>
        {t('tradeinfo-trade-comissions-p-3')}
      </p>
      <h3>{t('tradeinfo-trade-comissions-h-4')}</h3>
      <p>
        {t('tradeinfo-trade-comissions-p-4')}
      </p>
    </div>
  )
}

export default withNamespaces()(TradeComissions)
