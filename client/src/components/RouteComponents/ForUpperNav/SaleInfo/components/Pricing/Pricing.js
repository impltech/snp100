// react
import React from 'react'

// styles
import './Pricing.css'

import {t} from 'i18next'
import {withNamespaces} from 'react-i18next'

const Pricing = () => {
  return (
    <div className="pricing">
      <h3>{t('tradeinfo-pricing-h-1')}</h3>
      <p>
        {t('tradeinfo-pricing-p-1.p-1')}
      </p>
      <p>
        {t('tradeinfo-pricing-p-1.p-2')}
      </p>
      <h3>{t('tradeinfo-pricing-h-2')}</h3>
      <p>
        {t('tradeinfo-pricing-p-2')}
      </p>
      <h3>
        {t('tradeinfo-pricing-h-3')}
      </h3>
      <p>
        {t('tradeinfo-pricing-p-3.p-1')}
      </p>
      <p>
        {t('tradeinfo-pricing-p-3.p-2')}
      </p>
      <p>
        {t('tradeinfo-pricing-p-3.p-3')}
      </p>
      <h3>
        {t('tradeinfo-pricing-h-4')}
      </h3>
      <p>
        {t('tradeinfo-pricing-p-4.p-1')}
      </p>
      <p>
        {t('tradeinfo-pricing-p-4.p-2')}
      </p>
      <h3>{t('tradeinfo-pricing-h-5')}</h3>
      <p>
        {t('tradeinfo-pricing-p-5')}
      </p>
    </div>
  )
}

export default withNamespaces()(Pricing)
