// react
import React from 'react'

// styles
import './Leverage.css'

import {t} from 'i18next'
import {withNamespaces} from 'react-i18next'

const Leverage = () => {
  return (
    <div className="leverage">
      <h3>{t('tradeinfo-margin-trade-h-1')}</h3>
      <p>
        {t('tradeinfo-margin-trade-p-1.p-1')}
      </p>
      <p>
        {t('tradeinfo-margin-trade-p-1.p-2')}
      </p>
      <p>
        {t('tradeinfo-margin-trade-p-1.p-3')}
      </p>
      <h3>{t('tradeinfo-margin-trade-h-2')}</h3>
      <p>
        {t('tradeinfo-margin-trade-p-2.p-1')}
      </p>
      <p>
        {t('tradeinfo-margin-trade-p-2.p-2')}
      </p>
      <p>
        {t('tradeinfo-margin-trade-p-2.p-3')}
      </p>
      <h3>{t('tradeinfo-margin-trade-h-3')}</h3>
      <p>
        {t('tradeinfo-margin-trade-p-3.p-1')}
      </p>
      <p>
        {t('tradeinfo-margin-trade-p-3.p-2')}
      </p>
      <p>
        {t('tradeinfo-margin-trade-p-3.p-3')}
      </p>
      <p>
        {t('tradeinfo-margin-trade-p-3.p-4')}
      </p>
      <p>
        {t('tradeinfo-margin-trade-p-3.p-5')}
      </p>
      <p>
        {t('tradeinfo-margin-trade-p-3.p-6')}
      </p>
      <p>
        {t('tradeinfo-margin-trade-p-3.p-7')}
      </p>
      <p>
        {t('tradeinfo-margin-trade-p-3.p-8')}
      </p>
      <p>
        {t('tradeinfo-margin-trade-p-3.p-9')}
      </p>
      <h3>{t('tradeinfo-margin-trade-h-4')}</h3>
      <p>
        {t('tradeinfo-margin-trade-p-4.p-1')}
      </p>
      <p>
        {t('tradeinfo-margin-trade-p-4.p-2')}
      </p>
      <h3>{t('tradeinfo-margin-trade-h-5')}</h3>
      <p>
        {t('tradeinfo-margin-trade-p-5.p-1')}
      </p>
      <p>
        {t('tradeinfo-margin-trade-p-5.p-2')}
      </p>
      <h3>{t('tradeinfo-margin-trade-h-6')}</h3>
      <p>
        {t('tradeinfo-margin-trade-p-6')}
      </p>
    </div>
  )
}

export default withNamespaces()(Leverage)
