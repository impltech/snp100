// react
import React from 'react'

import {withNamespaces} from 'react-i18next'
import {t} from 'i18next'

// styles
import './Credits.css'

const Credits = () => {
  return (
    <div className="credits">
      <h3>{t('tradeinfo-credits-h-1')}</h3>
      <p>
        {t('tradeinfo-credits-p-1')}
      </p>
      <h3>{t('tradeinfo-credits-h-2')}</h3>
      <ul>
        <li>
          {t('tradeinfo-credits-li-1')}
        </li>
        <li>
          {t('tradeinfo-credits-li-2')}
        </li>
        <li>
          {t('tradeinfo-credits-li-3')}
        </li>
        <li>
          {t('tradeinfo-credits-li-4')}
        </li>
        <li>
          {t('tradeinfo-credits-li-5')}
        </li>
        <li>
          {t('tradeinfo-credits-li-6')}
        </li>
        <li>
          {t('tradeinfo-credits-li-7')}
        </li>
        <li>
          {t('tradeinfo-credits-li-8')}
        </li>
        <li>
          {t('tradeinfo-credits-li-9')}
        </li>
        <li>
          {t('tradeinfo-credits-li-10')}
        </li>
        <li>
          {t('tradeinfo-credits-li-11')}
        </li>
        <li>
          {t('tradeinfo-credits-li-12')}
        </li>
        <li>
          {t('tradeinfo-credits-li-13')}
        </li>
      </ul>
    </div>
  )
}

export default withNamespaces()(Credits)
