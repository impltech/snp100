// react
import React from 'react'

// styles
import './SaleWithSnp.css'

import {withNamespaces} from 'react-i18next'
import {t} from 'i18next'

const SaleWithSnp = () => {
  return (
    <div className="sale-with-snp">
      <p>{t('tradeinfo-trade-with-C&P100-p-1.p-1')}
      </p>
      <p>{t('tradeinfo-trade-with-C&P100-p-1.p-2')}</p>
      <h3>{t('tradeinfo-trade-with-C&P100-h-1')}</h3>
      <p>
        {t('tradeinfo-trade-with-C&P100-p-2.p-1')}
      </p>
      <p>
        {t('tradeinfo-trade-with-C&P100-p-2.p-2')}
      </p>
      <p>
        {t('tradeinfo-trade-with-C&P100-p-2.p-3')}
      </p>
      <ul>
        <li>{t('tradeinfo-trade-with-C&P100-li-1')}</li>
        <li>
          {t('tradeinfo-trade-with-C&P100-li-2')}
        </li>
        <li>
          {t('tradeinfo-trade-with-C&P100-li-3')}
        </li>
        <li>{t('tradeinfo-trade-with-C&P100-li-4')}</li>
        <li>
          {t('tradeinfo-trade-with-C&P100-li-5')}
        </li>
      </ul>
      <p/>
      <h3>{t('tradeinfo-trade-with-C&P100-h-2')}</h3>
      <p>
        {t('tradeinfo-trade-with-C&P100-p-3')}
      </p>
    </div>
  )
}

export default withNamespaces()(SaleWithSnp)
