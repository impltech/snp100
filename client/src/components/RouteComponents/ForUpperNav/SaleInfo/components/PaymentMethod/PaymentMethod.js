// react
import React from 'react'

// styles
import './PaymentMethod.css'

import {withNamespaces} from 'react-i18next'
import {t} from 'i18next'

const PaymentMethod = () => {
  return (
    <div className="payment-method">
      <p>
        {t('tradeinfo-payment-method-p')}
      </p>
    </div>
  )
}

export default withNamespaces()(PaymentMethod)
