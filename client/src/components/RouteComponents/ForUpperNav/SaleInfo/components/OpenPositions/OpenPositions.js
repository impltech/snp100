// react
import React from 'react'

// styles
import './OpenPositions.css'

import {withNamespaces} from 'react-i18next'
import {t} from 'i18next'

const OpenPositions = () => {
  return (
    <div className="open-positions">
      <p>
        {t('tradeinfo-open-position-p-1.p-1')}
      </p>
      <p>
        {t('tradeinfo-open-position-p-1.p-2')}
      </p>
      <ol>
        <li>{t('tradeinfo-open-position-ol-li-1')}</li>
        <li>{t('tradeinfo-open-position-ol-li-2')}</li>
        <li>{t('tradeinfo-open-position-ol-li-3')}</li>
      </ol>
      <p>
        {t('tradeinfo-open-position-p-2.p-1')}
      </p>
      <p>
        {t('tradeinfo-open-position-p-2.p-2')}
      </p>
      <p>
        {t('tradeinfo-open-position-p-2.p-3')}
      </p>
      <p>
        {t('tradeinfo-open-position-p-2.p-4')}
      </p>
      <h3>{t('tradeinfo-how-open-pos')}</h3>
      <p>
        {t('tradeinfo-how-open-pos-p-1.p-1')}
      </p>
      <p>
        {t('tradeinfo-how-open-pos-p-1.p-2')}
      </p>
      <p>{t('tradeinfo-how-open-pos-p-1.p-3')}</p>
      <ul>
        <li>{t('tradeinfo-how-open-pos-li-1')}</li>
        <li>{t('tradeinfo-how-open-pos-li-2')}</li>
        <li>{t('tradeinfo-how-open-pos-li-3')}</li>
        <li>{t('tradeinfo-how-open-pos-li-4')}</li>
        <li>
          {t('tradeinfo-how-open-pos-li-5')}
        </li>
      </ul>
      <p>
        {t('tradeinfo-how-open-pos-p-2.p-1')}
      </p>
      <p>
        {t('tradeinfo-how-open-pos-p-2.p-2')}
      </p>
      <p>
        {t('tradeinfo-how-open-pos-p-2.p-3')}
      </p>
      <p>
        {t('tradeinfo-how-open-pos-p-2.p-4')}
      </p>
    </div>
  )
}

export default withNamespaces()(OpenPositions)
