// react
import React from 'react'

// styles
import './OrderTypes.css'

import {t} from 'i18next'
import {withNamespaces} from 'react-i18next'

const OrderTypes = () => {
  return (
    <div className="order-types">
      <h3>{t('tradeinfo-order-types-h-1')}</h3>
      <p>
        {t('tradeinfo-order-types-p-1')}
      </p>
      <h3>{t('tradeinfo-order-types-h-2')}</h3>
      <p>
        {t('tradeinfo-order-types-p-2.p-1')}
      </p>
      <p>
        {t('tradeinfo-order-types-p-2.p-2')}
      </p>
      <p>
        {t('tradeinfo-order-types-p-2.p-3')}
      </p>
      <h3>{t('tradeinfo-order-types-h-3')}</h3>
      <p>
        {t('tradeinfo-order-types-p-3')}
      </p>
    </div>
  )
}

export default withNamespaces()(OrderTypes)
