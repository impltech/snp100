// react
import React from 'react'

// translation
import {withNamespaces} from 'react-i18next'
import {t} from 'i18next'

// styles
import './ContractsForDiff.css'

const ContractsForDiff = () => {
  return (
    <div className="sale-contacts">
      <h3>{t('tradeinfo-cdf-h1')}</h3>
      <p>
        {t('tradeinfo-cdf-p1.p-1')}
      </p>
      <p>
        {t('tradeinfo-cdf-p1.p-2')}
      </p>
      <p>
        {t('tradeinfo-cdf-p1.p-3')}
      </p>
      <p>
        {t('tradeinfo-cdf-p1.p-4')}
      </p>
      <h3>{t('tradeinfo-cdf-h2')}</h3>
      <p>
        {t('tradeinfo-cdf-p2')}
      </p>
    </div>
  )
}

export default withNamespaces()(ContractsForDiff)
