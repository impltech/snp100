// react
import React from 'react'

import {withNamespaces} from 'react-i18next'
import {t} from 'i18next'

// styles
import './WithdrawalOfMoney.css'

const WithdrawalOfMoney = () => {
  return (
    <div className="withdrawal">
      <p>
        {t('tradeinfo-withdrawal-p')}
      </p>
      <h3>{t('tradeinfo-withdrawal-h-1')}</h3>
      <p>
        {t('tradeinfo-withdrawal-p-1.p-1')}
      </p>
      <p>
        {t('tradeinfo-withdrawal-p-1.p-2')}
      </p>
      <p>
        {t('tradeinfo-withdrawal-p-1.p-3')}
      </p>
      <p>
        {t('tradeinfo-withdrawal-p-1.p-4')}
      </p>
      <p>
        {t('tradeinfo-withdrawal-p-1.p-5')}
      </p>
      <h3>{t('tradeinfo-withdrawal-h-2')}</h3>
      <p>
        {t('tradeinfo-withdrawal-p-2')}
      </p>
      <h3>{t('tradeinfo-withdrawal-h-3')}</h3>
      <p>
        {t('tradeinfo-withdrawal-p-3')}
      </p>
    </div>
  )
}

export default withNamespaces()(WithdrawalOfMoney)
