// react
import React from 'react'
import {Link} from 'react-router-dom'
import {t} from 'i18next'
import {withNamespaces} from 'react-i18next'

// title info
import DocumentTitle from 'react-document-title'

// hocs
import Layout from '../../../../hocs/Layout/Layout'
import arrow from '../../../../assets/innerPages/angle-arrow-down.png'

import './AccountsTypes.css'
import './BgColors.css'

const AccountsTypes = () => {
  return (
    <Layout>
      <DocumentTitle title={t('accounts-types-H')}>
        <div className="content-block-accounts">
          <div className="in-block-nav">
            <Link to="/">{t('main')}</Link>
            <img src={arrow} alt='' className={'arrow-right'}/>
            {t('accounts-types-H')}
          </div>
          <h1>{t('accounts-types-H')}</h1>
          <section>
            <div>
              <p>{t('accounts-types-p-1')}</p>
              <table cellPadding="0" cellSpacing="0" className='accounts-tbl'>
                <tbody>
                  <tr className="height_55">
                    <td colSpan="5">&nbsp;</td>
                    <td colSpan="2"><span className="font_14 color_grey">{t('popular')}</span></td>
                    <td colSpan="2"><span className="font_14 color_grey">{t('recommended')}</span></td>
                    <td colSpan="2">&nbsp;</td>
                  </tr>
                  <tr className="height_55">
                    <td className="width_250 bg_grey"><span className="font_20 color_grey ">{t('service-level')}</span></td>
                    <td className="bg_basic-white" colSpan="2"><span
                      className="text_to-uppercase font_CirceRegular color_white font_16">{t('Basic')}</span></td>
                    <td className="bg_gold" colSpan="2"><span
                      className="text_to-uppercase font_CirceRegular color_white font_16">{t('Gold')}</span></td>
                    <td className="bg_platinum" colSpan="2"><span
                      className="text_to-uppercase font_CirceRegular color_white font_16">{t('Platinum')}</span></td>
                    <td className="bg_blue" colSpan="2"><span
                      className="text_to-uppercase font_CirceRegular color_white font_16">{t('Exclusive')}</span></td>
                    <td className="bg_vip-blue" colSpan="2"><span
                      className="text_to-uppercase font_CirceRegular color_white font_16">{t('VIP')}</span></td>
                  </tr>
                  <tr className="height_35">
                    <td className="width_250 bg_light-grey accounts-tbl-delimiter" rowSpan="4"><span
                      className="font_20 color_grey  display_block">{t('sum-on-account')}</span> <span
                      className="color_red display_block margin-t_25">{t('bonus')}</span></td>
                    <td className="width_70 bg_basic-white-sum"><span
                      className="color_grey font_14 display_block">{t('sum')}</span></td>
                    <td className="width_70 bg_basic-white-bonus"><span
                      className="display_inline-block font_14 color_red">{t('bonus')}</span></td>
                    <td className="width_70 bg_gold-sum"><span className="color_grey font_14 display_block">{t('sum')}</span>
                    </td>
                    <td className="width_70 bg_gold-bonus"><span
                      className="display_inline-block font_14 color_red">{t('bonus')}</span></td>
                    <td className="width_70 bg_platinum-sum"><span
                      className="color_grey font_14 display_block">{t('sum')}</span>
                    </td>
                    <td className="width_70 bg_platinum-bonus"><span
                      className="display_inline-block font_14 color_red">{t('bonus')}</span></td>
                    <td className="width_85 bg_blue-sum"><span
                      className="color_white-exclusive font_14 display_block">{t('sum')}</span></td>
                    <td className="width_85 bg_blue-bonus"><span
                      className="display_inline-block font_14 color_red">{t('bonus')}</span></td>
                    <td className="width_90 bg_vip-blue-sum"><span
                      className="font_14 color_white display_block">{t('sum')}</span>
                    </td>
                    <td className="width_90 bg_vip-blue-bonus"><span
                      className="-bonus-vip-txt font_14 color_vip-red">{t('bonus')}</span></td>
                  </tr>
                  <tr className="height_55">
                    <td className="width_70 bg_basic-white-sum"><span
                      className="color_grey font_14 font_14 display_block">€ 100</span>
                    </td>
                    <td className="width_70 bg_basic-white-bonus"><span
                      className="display_inline-block font_14 color_red">€ 50</span>
                    </td>
                    <td className="width_70 bg_gold-sum"><span className="color_grey font_14 display_block">€ 500</span>
                    </td>
                    <td className="width_70 bg_gold-bonus"><span
                      className="display_inline-block font_14 color_red">€ 200</span></td>
                    <td className="width_70 bg_platinum-sum"><span
                      className="color_grey font_14 display_block">€ 5 000</span>
                    </td>
                    <td className="width_70 bg_platinum-bonus"><span className="display_inline-block font_14 color_red">€ 2 000</span>
                    </td>
                    <td className="width_85 bg_blue-sum"><span className="color_white-exclusive font_14 display_block">€ 50 000</span>
                    </td>
                    <td className="width_85 bg_blue-bonus"><span
                      className="display_inline-block font_14 color_red">€ 20k</span></td>
                    <td className="width_90 bg_vip-blue-sum"><span
                      className="font_14 color_white display_block">€ 500 000</span></td>
                    <td className="width_90 bg_vip-blue-bonus"><span
                      className="-bonus-vip-txt font_14 color_vip-red">€ 200k</span></td>
                  </tr>
                  <tr className="height_55">
                    <td className="width_70 bg_basic-white-sum" rowSpan="2"><span
                      className="color_grey font_14 display_block">€ 250</span></td>
                    <td className="width_70 bg_basic-white-bonus" rowSpan="2"><span
                      className="display_inline-block font_14 color_red">€ 100</span></td>
                    <td className="width_70 bg_gold-sum"><span className="color_grey font_14 display_block">€ 1 000</span>
                    </td>
                    <td className="width_70 bg_gold-bonus"><span
                      className="display_inline-block font_14 color_red">€ 400</span></td>
                    <td className="width_70 bg_platinum-sum"><span
                      className="color_grey font_14 display_block">€ 10 000</span></td>
                    <td className="width_70 bg_platinum-bonus"><span className="display_inline-block font_14 color_red">€ 3 500</span>
                    </td>
                    <td className="width_85 bg_blue-sum"><span className="color_white-exclusive font_14 display_block">€ 100 000</span>
                    </td>
                    <td className="width_85 bg_blue-bonus"><span
                      className="display_inline-block font_14 color_red">€ 40k</span></td>
                    <td className="width_90 bg_vip-blue-sum"><span
                      className="font_14 color_white display_block">€ 750 000</span></td>
                    <td className="width_90 bg_vip-blue-bonus"><span
                      className="-bonus-vip-txt font_14 color_vip-red">€ 300k</span></td>
                  </tr>
                  <tr className="height_55">
                    <td className="width_70 bg_gold-sum"><span className="color_grey font_14 display_block">€ 2 500</span>
                    </td>
                    <td className="width_70 bg_gold-bonus"><span
                      className="display_inline-block font_14 color_red">€ 1 000</span></td>
                    <td className="width_70 bg_platinum-sum"><span
                      className="color_grey font_14 display_block">€ 25 000</span></td>
                    <td className="width_70 bg_platinum-bonus"><span className="display_inline-block font_14 color_red">€ 10 000</span>
                    </td>
                    <td className="width_85 bg_blue-sum"><span className="color_white-exclusive font_14 display_block">€ 250 000</span>
                    </td>
                    <td className="width_85 bg_blue-bonus"><span
                      className="display_inline-block font_14 color_red">€ 100k</span></td>
                    <td className="width_90 bg_vip-blue-sum"><span
                      className="font_14 color_white display_block">€ 1 000 000</span></td>
                    <td className="width_90 bg_vip-blue-bonus"><span
                      className="-bonus-vip-txt font_14 color_vip-red">€ 400k</span></td>
                  </tr>
                  <tr className="height_60">
                    <td className="height_60" colSpan="11"><span
                      className="color_grey text_to-uppercase font_20">{t('finance')}</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_grey"><span className="width_210 display_inline-block color_grey font_16">{t('percents')}</span>
                    </td>
                    <td className="bg_basic-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_gold-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_platinum-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_exclusive-plus" colSpan="2"><span
                      className="color_exclusive-plus tbl-plus-txt display_block display_block">+</span></td>
                    <td className="bg_vip-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block display_block">+</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_light-grey"><span
                      className="width_210 display_inline-block color_grey font_16">{t('compensation')}</span>
                    </td>
                    <td className="bg_basic-light-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_gold-light-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_platinum-light-plus" colSpan="2"><span
                      className="color_platinum-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_exclusive-light-plus" colSpan="2"><span
                      className="color_exclusive-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_vip-light-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block">+</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_grey"><span className="width_210 display_inline-block color_grey font_16">{t('individual')}</span>
                    </td>
                    <td className="bg_basic-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_gold-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_platinum-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_exclusive-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_vip-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block">+</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_light-grey"><span
                      className="width_210 display_inline-block color_grey  font_16">{t('trust')}</span></td>
                    <td className="bg_basic-light-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_gold-light-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_platinum-light-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_exclusive-light-plus" colSpan="2"><span
                      className="color_exclusive-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_vip-light-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block">+</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_grey"><span className="width_210 display_inline-block color_red font_16">{t('insurance')}</span>
                    </td>
                    <td className="bg_basic-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_gold-plus" colSpan="2"><span
                      className="display_inline-block font_14 font_14 color_red width_110">{t('upto40')}&nbsp;<span
                        className="tbl-no-wrap display_block">{t('from-deposit')}</span></span></td>
                    <td className="bg_platinum-plus" colSpan="2"><span
                      className="display_inline-block font_14 color_red width_110">{t('upto50')}<span
                        className="tbl-no-wrap display_block">{t('from-deposit')}</span></span>
                    </td>
                    <td className="bg_exclusive-plus" colSpan="2"><span
                      className="display_inline-block font_14 color_red width_85">{t('upto70')} <span
                        className="tbl-no-wrap display_block">{t('from-deposit')}</span></span>
                    </td>
                    <td className="bg_vip-plus" colSpan="2"><span
                      className="display_inline-block font_14 color_red width_85">{t('upto100')} <span
                        className="tbl-no-wrap">{t('from-deposit')}</span></span></td>
                  </tr>
                  <tr className="height_60">
                    <td className="height_60" colSpan="11"><span
                      className="color_grey text_to-uppercase font_20">{t('supply')}</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_grey"><span className="width_210 display_inline-block color_grey font_16">{t('consultation')}</span>
                    </td>
                    <td className="bg_basic-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_gold-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_platinum-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_exclusive-plus" colSpan="2"><span
                      className="color_exclusive-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_vip-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block">+</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_light-grey"><span
                      className="width_210 display_inline-block color_grey font_16">{t('making-deals')} <span
                        className="tbl-no-wrap">{t('by-phone')}</span></span></td>
                    <td className="bg_basic-light-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_gold-light-plus" colSpan="2"><span
                      className="color_gold-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_platinum-light-plus" colSpan="2"><span
                      className="color_platinum-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_exclusive-light-plus" colSpan="2"><span
                      className="color_exclusive-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_vip-light-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block">+</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_grey"><span className="width_210 display_inline-block color_grey font_16">{t('sms')}</span>
                    </td>
                    <td className="bg_basic-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_gold-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_platinum-plus" colSpan="2"><span
                      className="color_platinum-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_exclusive-plus" colSpan="2"><span
                      className="color_exclusive-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_vip-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block">+</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_light-grey"><span
                      className="width_210 display_inline-block color_grey font_16">{t('personal')}</span></td>
                    <td className="bg_basic-light-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_gold-light-plus" colSpan="2"><span
                      className="color_gold-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_platinum-light-plus" colSpan="2"><span
                      className="color_platinum-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_exclusive-light-plus" colSpan="2"><span
                      className="color_exclusive-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_vip-light-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block">+</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_grey"><span className="width_210 display_inline-block color_grey font_16">{t('direct')} <span
                      className="tbl-no-wrap">{t('main-analytic')}</span></span></td>
                    <td className="bg_basic-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_gold-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_platinum-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_exclusive-plus" colSpan="2"><span
                      className="color_exclusive-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_vip-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block">+</span></td>
                  </tr>
                  <tr className="height_60">
                    <td className="height_60" colSpan="11"><span className="color_grey text_to-uppercase font_20">{t('ready')}</span>
                    </td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_grey"><span className="width_210 display_inline-block color_grey font_16">{t('ideas')}</span>
                    </td>
                    <td className="bg_basic-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_gold-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_platinum-plus" colSpan="2"><span
                      className="color_platinum-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_exclusive-plus" colSpan="2"><span
                      className="color_exclusive-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_vip-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block">+</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_light-grey"><span
                      className="width_210 display_inline-block color_grey font_16">{t('trade-systems')}</span></td>
                    <td className="bg_basic-light-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_gold-light-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_platinum-light-plus" colSpan="2"><span
                      className="color_platinum-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_exclusive-light-plus" colSpan="2"><span
                      className="color_exclusive-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_vip-light-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block">+</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_grey"><span className="width_210 display_inline-block color_grey font_16">{t('model')}</span>
                    </td>
                    <td className="bg_basic-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_gold-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_platinum-plus" colSpan="2"><span
                      className="color_platinum-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_exclusive-plus" colSpan="2"><span
                      className="color_exclusive-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_vip-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block">+</span></td>
                  </tr>
                  <tr className="height_60">
                    <td className="height_60" colSpan="11"><span
                      className="color_grey text_to-uppercase font_20">{t('learn')}</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_grey"><span className="width_210 display_inline-block color_grey font_16">{t('entrance-seminars')}</span>
                    </td>
                    <td className="bg_basic-plus" colSpan="2"><span
                      className="color_basic-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_gold-plus" colSpan="2"><span
                      className="color_gold-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_platinum-plus" colSpan="2"><span
                      className="color_platinum-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_exclusive-plus" colSpan="2"><span
                      className="color_exclusive-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_vip-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block">+</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_light-grey"><span
                      className="width_210 display_inline-block color_grey font_16">{t('base-course')}</span>
                    </td>
                    <td className="bg_basic-light-plus" colSpan="2"><span
                      className="color_basic-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_gold-light-plus" colSpan="2"><span
                      className="color_gold-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_platinum-light-plus" colSpan="2"><span
                      className="color_platinum-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_exclusive-light-plus" colSpan="2"><span
                      className="color_exclusive-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_vip-light-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block">+</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_grey"><span className="width_210 display_inline-block color_grey font_16">{t('advance-course')}</span>
                    </td>
                    <td className="bg_basic-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_gold-plus" colSpan="2"><span
                      className="color_gold-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_platinum-plus" colSpan="2"><span
                      className="color_platinum-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_exclusive-plus" colSpan="2"><span
                      className="color_exclusive-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_vip-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block">+</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_light-grey"><span
                      className="width_210 display_inline-block color_grey font_16">{t('trade-webinars')}&nbsp; <span
                        className="tbl-no-wrap display_block">{t('currency')}</span> &nbsp;{t('tools')}</span></td>
                    <td className="bg_basic-light-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_gold-light-plus" colSpan="2"><span
                      className="color_gold-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_platinum-light-plus" colSpan="2"><span
                      className="color_platinum-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_exclusive-light-plus" colSpan="2"><span
                      className="color_exclusive-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_vip-light-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block">+</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_grey">
                      <span className="width_210 display_inline-block color_grey font_16">{t('trade-webinars')}&nbsp; <span
                        className="tbl-no-wrap">{t('trade-goods')}</span> {t('market')}</span></td>
                    <td className="bg_basic-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_gold-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_platinum-plus" colSpan="2"><span
                      className="color_platinum-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_exclusive-plus" colSpan="2"><span
                      className="color_exclusive-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_vip-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block">+</span></td>
                  </tr>
                  <tr className="height_70">
                    <td className="width_250 bg_light-grey"><span
                      className="width_210 display_inline-block color_grey font_16">{t('trade-webinars')}&nbsp;<span className='display_block'>{t('founds-market')}</span> </span>
                    </td>
                    <td className="bg_basic-light-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_gold-light-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_platinum-light-plus" colSpan="2">&nbsp;</td>
                    <td className="bg_exclusive-light-plus" colSpan="2"><span
                      className="color_exclusive-plus tbl-plus-txt display_block">+</span></td>
                    <td className="bg_vip-light-plus" colSpan="2"><span
                      className=".color_vip-plus tbl-plus-txt display_block">+</span></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </section>
        </div>
      </DocumentTitle>
    </Layout>
  )
}

export default withNamespaces()(AccountsTypes)
