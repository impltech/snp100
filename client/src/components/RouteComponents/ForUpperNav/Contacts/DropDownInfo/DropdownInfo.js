import React, {Component} from 'react'
import {t} from 'i18next'
import {withNamespaces} from 'react-i18next'
import close from '../../../../../assets/materials/back.svg'

class DropdownInfo extends Component {
  render () {
    const relative = {'position': 'relative'}
    const office = this.props.office
    const info = document.getElementById('dropdown-info')
    if (info) info.className = 'dropdown-info'
    return (
      <div className="dropdown-info" id='dropdown-info'>
        <div style={relative}>
          <div className="close-btn" onClick={this.props.close}>
            <img src={close} alt=""/>
          </div>
        </div>
        <h2>{t(office.branch_name)}</h2>
        {/*<p className="info-header">{t('branch_phone')}{' '}<b>{office.branch_phone}</b></p>*/}
        {/*<p className="info-header">{t('branch_fax')}<b>{office.branch_fax}</b></p>*/}
        <p className="info-header">{t('branch-address')}{' '}{t(office.branch_address)}</p>
        <span className="info-header">{t('branch_mail')} </span>
        <a className='info' href={'mailto://' + office.branch_mail}>{office.branch_mail}</a>

        <p className='info-p'>{t(office.branch_info_p_1)}</p>
        <p className='info-p'>{t(office.branch_info_p_2)}</p>
        <p className='info-p'>{t(office.branch_info_p_3)}</p>
        {office.branch_info_li_1 && <li>{t(office.branch_info_li_1)}</li>}
        {office.branch_info_li_2 && <li>{t(office.branch_info_li_2)}</li>}
        {office.branch_info_li_3 && <li>{t(office.branch_info_li_3)}</li>}
        {office.branch_info_li_4 && <li>{t(office.branch_info_li_4)}</li>}
        {office.branch_info_li_5 && <li>{t(office.branch_info_li_5)}</li>}
        {office.branch_info_li_6 && <li>{t(office.branch_info_li_6)}</li>}
      </div>
    )
  }
}

export default withNamespaces()(DropdownInfo)