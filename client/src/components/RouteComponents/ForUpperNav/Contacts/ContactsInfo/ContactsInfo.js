// react
import React from 'react'
import {t} from 'i18next'

// styles
import './ContactsInfo.css'
import {withNamespaces} from 'react-i18next'
import {Link} from 'react-router-dom'

const ContactsInfo = () => {
  return (
    <div className="contacts-info">
      <h2>{t('Profile-services')}</h2>

      {/* <p className="info-header">{t('Fax')}</p> */}
      {/* <Link to={'#'} className="info">{'+7 495 111 11 11'}</Link> */}

      <p className="info-header">{t('common-info')}</p>
      <Link to={'#'} className="info">{'info@snp100.com'}</Link>

      <p className="info-header">{t('support')}</p>
      <Link to={'#'} className="info">{'support@snp100.com'}</Link>

      <p className="info-header">{t('finances-ets')}</p>
      <Link to={'#'} className="info">{'billing@snp100.com'}</Link>

      <p className="info-header">{t('marketing')}</p>
      <Link to={'#'}className="info">{'marketing@snp100.com'}</Link>

      <p className="info-header">{t('partners-service')}</p>
      <Link to={'#'} className="info">{'affiliates@snp100.com'}</Link>

      <p className="info-header">{t('Withdrawal')}</p>
      <Link to={'#'} className="info">{'withdrawals@snp100.com'}</Link>

      <p className="info-header">{t('Complaints')}</p>
      <Link to={'#'} className="info">{'compliance@snp100.com'}</Link>
      <h2>{t('BUSINESS HOURS')}</h2>
      <p className="info-header">{t('Opening')}</p>
      <p className="info-header">{t('opening-hours')}</p>
      <p className="info-header">{t('Closing')}</p>
      <p className="info-header">{t('closing-hours')}</p>
    </div>
  )
}

export default withNamespaces()(ContactsInfo)
