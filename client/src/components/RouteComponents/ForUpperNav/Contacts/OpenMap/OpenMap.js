import React, {Component} from 'react'
import {render} from 'react-dom'
import {Map as LeafletMap, TileLayer, Marker} from 'react-leaflet'

import './OpenMap.css'
import {offices} from '../offices'
import DropdownInfo from '../DropDownInfo/DropdownInfo'

class OpenMap extends Component {
  showInfo (o) {
    const container = document.getElementById('map-container')
    render(<DropdownInfo office={o} close={this.hideInfo}/>, container)
  }

  hideInfo () {
    const info = document.getElementById('dropdown-info')
    if (info) info.className = 'dropdown-info-hidden'
  }

  render () {
    return (
      <div>
        <LeafletMap
          center={[50, 50.4]}
          zoom={4}
          maxZoom={18}
          minZoom={3}
          attributionControl={true}
          zoomControl={true}
          doubleClickZoom={true}
          scrollWheelZoom={true}
          dragging={true}
          animate={true}
          easeLinearity={0.35}
          onClick={this.hideInfo}
        >
          <TileLayer
            url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
          />
          {offices[0] && offices.map(office =>
            <Marker position={[office.position.lat, office.position.lng]} onClick={() => this.showInfo(office)}>
            </Marker>
          )}

        </LeafletMap>
      </div>
    )
  }
}

export default OpenMap