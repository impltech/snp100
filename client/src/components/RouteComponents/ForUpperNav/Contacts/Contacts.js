// react
import React from 'react'
import {t} from 'i18next'
import Loadable from 'react-loadable'

// title info
import DocumentTitle from 'react-document-title'

// styles
import './Contacts.css'

// hocs
import Layout from '../../../../hocs/Layout/Layout'

// components
import {withNamespaces} from 'react-i18next'
import LoadingComponent from '../../../LoadingComponent/LoadingComponent'

const OpenMap = Loadable({
  loader: () => import('./OpenMap/OpenMap'),
  loading: LoadingComponent
})

const ContactsInfo = Loadable({
  loader: () => import('./ContactsInfo/ContactsInfo'),
  loading: LoadingComponent
})

const Contacts = () => {
  return (
    <Layout>
      <DocumentTitle title={t('contacts')}>
        <div className="contacts-block">
          <div className="map-container">
            <div className='dropdown-info-container' id='map-container'></div>
            <div className="map-block">
              <OpenMap/>
            </div>
          </div>
          <ContactsInfo/>
        </div>
      </DocumentTitle>
    </Layout>
  )
}

export default withNamespaces()(Contacts)
