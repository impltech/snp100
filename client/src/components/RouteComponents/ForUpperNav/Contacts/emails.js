export const emails = {
  info: 'info@snp100.com',
  support: 'support@snp100.com',
  billing: 'billing@snp100.com',
  marketing: 'marketing@snp100.com',
  partners: 'affiliates@snp100.com',
  withdrawal: 'withdrawals@snp100.com',
  compliance: 'compliance@snp100.com'
}