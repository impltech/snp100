// react
import React from 'react'

// styles
import './HelpColumn.css'
import TradingViewWidget, {Themes} from 'react-tradingview-widget'

const HelpColumn = () => {
  return (
    <div className="right-column">
      <div>
        <TradingViewWidget
          symbol="EURUSD"
          theme={Themes.LIGHT}
          locale="en"
          width="100%"
          height="280"
          hide_legend='true'
          style='3'
        />
      </div>
      <div>
        <TradingViewWidget
          symbol="GOLD"
          theme={Themes.LIGHT}
          locale="en"
          width="100%"
          height="280"
          hide_legend='true'
          style='5'
        />
      </div>
      <div>
        <TradingViewWidget
          symbol="WTI"
          theme={Themes.LIGHT}
          locale="en"
          width="100%"
          height="280"
          hide_legend='true'
          style='4'
        />
      </div>
    </div>
  )
}

export default HelpColumn
