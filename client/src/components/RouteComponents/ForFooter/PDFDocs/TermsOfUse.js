import React, {Component} from 'react'
import PDF from 'react-pdf-js'
import Layout from '../../../../hocs/Layout/Layout'
import risk from '../../../../assets/pdf/customer.pdf'
import './PDFContainer.css'

class TermsOfUse extends Component {
  state = {page: 1, pages: 1}

  onDocumentComplete = (pages) => {
    this.setState({page: 1, pages})
  }

  handlePrevious = () => {
    this.setState({page: this.state.page - 1})
  }

  handleNext = () => {
    this.setState({page: this.state.page + 1})
  }

  render () {
    const {page, pages} = this.state
    return (
      <div>
        <Layout>
          <div className="content-block">
            <section>
              <div className='pdf-container'>
                <button className="pdf-nav pdf-prev" disabled={page === 1} onClick={this.handlePrevious}></button>
                <PDF
                  file={risk}
                  onDocumentComplete={this.onDocumentComplete}
                  page={this.state.page}
                />
                <button className="pdf-nav pdf-next" disabled={page === pages} onClick={this.handleNext}></button>
              </div>
            </section>
          </div>
        </Layout>
      </div>
    )
  }
}

export default TermsOfUse