// react
import React from 'react'
import {Link} from 'react-router-dom'
import {t} from 'i18next'
import {withNamespaces} from 'react-i18next'

// title info
import DocumentTitle from 'react-document-title'

// hocs
import Layout from '../../../../hocs/Layout/Layout'

// components
import HelpColumn from '../../HelpColumn/HelpColumn'

// trading view
import TradingViewWidget, {Themes, BarStyles} from 'react-tradingview-widget'
import arrow from '../../../../assets/innerPages/angle-arrow-down.png'
import {registration} from '../../../../const/ApiSetting'

const Forex = () => {
  return (
    <Layout>
      <DocumentTitle title={t('forex')}>
        <div className="content-block">
          <div className="in-block-nav">
            <Link to="/">{t('main')}</Link>
            <img src={arrow} alt='' className={'arrow-right'}/>
            {t('forex')}
          </div>
          <h1>{t('forex')}</h1>
          <section>
            <div className="left-col">
              <TradingViewWidget
                symbol="EURUSD"
                theme={Themes.LIGHT}
                locale="en"
                barStyle={BarStyles.Baseline}
                width="100%"
                height="400"
              />

              <h3>{t('forex-h-1')}</h3>
              <p>{t('forex-p-1.p-1')}</p>
              <p>{t('forex-p-1.p-2')}</p>
              <p>{t('forex-p-1.p-3')}</p>
              <p>{t('forex-p-1.p-4')}</p>
              <h3>{t('forex-h-2')}</h3>
              <p>{t('forex-p-2')}{' '}
                <Link to='/saleinfo/diff_contracts'>{t('forex-link-1')}</Link></p>
              <h3>{t('forex-h-3')}</h3>
              <p>{t('forex-p-3')}</p>
              <h3>{t('forex-h-4')}</h3>
              <p>{t('forex-p-4')}</p>
              <h3>{t('forex-h-5')}</h3>
              <p>{t('forex-p-5')}</p>
              <p><a href={registration}>{' '}{t('forex-link-2')}</a>{t('forex-p-6')}</p>
            </div>
            <HelpColumn/>
          </section>
        </div>
      </DocumentTitle>
    </Layout>
  )
}

export default withNamespaces()(Forex)
