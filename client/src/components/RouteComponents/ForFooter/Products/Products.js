// react
import React from 'react'
import {Link} from 'react-router-dom'
import {t} from 'i18next'

// title info
import DocumentTitle from 'react-document-title'

// hocs
import Layout from '../../../../hocs/Layout/Layout'

// components
import HelpColumn from '../../HelpColumn/HelpColumn'

// trading view
import TradingViewWidget, {Themes, BarStyles} from 'react-tradingview-widget'
import {withNamespaces} from 'react-i18next'
import arrow from '../../../../assets/innerPages/angle-arrow-down.png'
import {registration} from '../../../../const/ApiSetting'

const Products = () => {
  return (
    <Layout>
      <DocumentTitle title={t('commodity')}>
        <div className="content-block">
          <div className="in-block-nav">
            <Link to="/">{t('main')}</Link>
            <img src={arrow} alt='' className={'arrow-right'}/>
            {t('commodity')}
          </div>
          <h1>{t('commodity')}</h1>
          <section>
            <div className="left-col">
              <TradingViewWidget
                symbol="GOLD"
                theme={Themes.DARK}
                locale="en"
                barStyle={BarStyles.AREA}
                width="100%"
                height="400"
              />
              <h3>
                {t('products-h-1')}
              </h3>
              <p>
                {t('products-p-1')}
              </p>

              <h3>
                {t('products-h-2')}
              </h3>
              <p>
                {t('products-p-2')}{' '}<Link to="/saleinfo/diff_contracts">{t('products-link-1')}</Link>
              </p>

              <h3>
                {t('products-h-3')}
              </h3>
              <p>
                {t('products-p-3')}
              </p>

              <h3> {t('products-h-4')}</h3>
              <p>
                {t('products-p-4')}
              </p>

              <h3> {t('products-h-5')}</h3>
              <p>
                {t('products-p-5')}
              </p>

              <p>
                <a href={registration}>{t('indexes-link-2')}</a>{' '}{t('products-p-6')}
              </p>
            </div>
            <HelpColumn/>
          </section>
        </div>
      </DocumentTitle>
    </Layout>
  )
}

export default withNamespaces()(Products)
