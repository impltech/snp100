// react
import React from 'react'
import { Link } from 'react-router-dom'
import {t} from 'i18next'
import {withNamespaces} from 'react-i18next'

// title info
import DocumentTitle from 'react-document-title'

// hocs
import Layout from '../../../../hocs/Layout/Layout'

// components
import HelpColumn from '../../HelpColumn/HelpColumn'

// trading view
import TradingViewWidget, { Themes, BarStyles } from 'react-tradingview-widget'
import arrow from '../../../../assets/innerPages/angle-arrow-down.png'

const CFD = () => {
  return (
    <Layout>
      <DocumentTitle title="CFD">
        <div className="content-block">
          <div className="in-block-nav">
            <Link to="/">{t('main')}</Link>
            <img alt='' src={arrow} className={'arrow-right'}/>
            {t('CFD')}
          </div>
          <h1>{t('CFD')}</h1>
          <section>
            <div className="left-col">
              <TradingViewWidget
                symbol="USOIL"
                theme={Themes.LIGHT}
                locale="en"
                barStyle={BarStyles.AREA}
                width="100%"
                height="400"
              />
              <h3>{t('CFD-h-1')}</h3>
              <p>{t('CFD-p-1.p-1')}</p>
              <p>{t('CFD-p-1.p-2')}</p>
              <p>{t('CFD-p-1.p-3')}</p>
              <p>{t('CFD-p-1.p-4')}</p>

              <h3>{t('CFD-h-2')}</h3>
              <p>{t('CFD-p-2.p-1')}</p>
              <p>{t('CFD-p-2.p-2')}</p>
              <p>{t('CFD-p-2.p-3')}</p>
              <p>{t('CFD-p-2.p-4')}</p>
              <p>{t('CFD-p-2.p-5')}</p>

              <h3>{t('CFD-h-3')}</h3>
              <p>{t('CFD-p-3')}</p>

              <h3>{t('CFD-h-4')}</h3>
              <li>{t('CFD-li-1')}</li>
              <li>{t('CFD-li-2')}</li>
              <li>{t('CFD-li-3')}</li>
              <li>{t('CFD-li-4')}</li>
              <li>{t('CFD-li-5')}</li>
            </div>
            <HelpColumn />
          </section>
        </div>
      </DocumentTitle>
    </Layout>
  )
}

export default withNamespaces()(CFD)
