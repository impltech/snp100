// react
import React from 'react'
import { Link } from 'react-router-dom'
import {t} from 'i18next'
import {withNamespaces} from 'react-i18next'

// title info
import DocumentTitle from 'react-document-title'

// hocs
import Layout from '../../../../hocs/Layout/Layout'

// components
import HelpColumn from '../../HelpColumn/HelpColumn'

// trading view
import TradingViewWidget, { Themes, BarStyles } from 'react-tradingview-widget'
import arrow from '../../../../assets/innerPages/angle-arrow-down.png'
import {registration} from '../../../../const/ApiSetting'

const Indexes = () => {
  return (
    <Layout>
      <DocumentTitle title={t('indexes')}>
        <div className="content-block">
          <div className="in-block-nav">
            <Link to="/">{t('main')}</Link>
            <img src={arrow} alt='' className={'arrow-right'}/>
            {t('indexes')}
          </div>
          <h1>{t('indexes')}</h1>
          <section>
            <div className="left-col">
              <TradingViewWidget
                symbol="DAX"
                theme={Themes.DARK}
                locale="en"
                barStyle={BarStyles.AREA}
                width="100%"
                height="400"
              />
              <h3>
                {t('indexes-h-1')}
              </h3>
              <p>
                {t('indexes-p-1')}
              </p>
              <h3>{t('indexes-h-2')}</h3>
              <p>
                {t('indexes-p-2')}
              </p>
              <h3>
                {t('indexes-h-3')}
              </h3>
              <p>
                {t('indexes-p-3')} {' '}
                <Link to="/saleinfo/diff_contracts">{t('indexes-link-1')}</Link>
              </p>
              <h3>
                {t('indexes-h-4')}
              </h3>
              <p>
                {t('indexes-p-4')}
              </p>
              <h3>{t('indexes-h-5')}</h3>
              <p>
                {t('indexes-p-5')}
              </p>
              <h3>{t('indexes-h-6')}</h3>
              <p>
                {t('indexes-p-6')}
              </p>
              <p>
                <a href={registration}>{t('indexes-link-2')}</a>{' '} {t('indexes-')}
              </p>
            </div>
            <HelpColumn />
          </section>
        </div>
      </DocumentTitle>
    </Layout>
  )
}

export default withNamespaces()(Indexes)
