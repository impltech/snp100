// react
import React from 'react'
import {Link} from 'react-router-dom'
import {t} from 'i18next'
import {withNamespaces} from 'react-i18next'

// title info
import DocumentTitle from 'react-document-title'

// hocs
import Layout from '../../../../hocs/Layout/Layout'

// components
import HelpColumn from '../../HelpColumn/HelpColumn'

// trading view
import TradingViewWidget, {Themes, BarStyles} from 'react-tradingview-widget'
import arrow from '../../../../assets/innerPages/angle-arrow-down.png'

const Stock = () => {
  return (
    <Layout>
      <DocumentTitle title={t('stocks')}>
        <div className="content-block">
          <div className="in-block-nav">
            <Link to="/">{t('main')}</Link>
            <img src={arrow} alt='' className={'arrow-right'}/>
            {t('stocks')}
          </div>

          <h1>{t('stocks')}</h1>
          <section>
            <div className="left-col">
              <TradingViewWidget
                symbol="AAPL"
                theme={Themes.LIGHT}
                locale="en"
                barStyle={BarStyles.AREA}
                width="100%"
                height="400"
              />
              <h3>
                {t('stock-h-1')}
              </h3>

              <p>
                {t('stock-p-1.p-1')}
              </p>
              <p>
                {t('stock-p-1.p-2')}
              </p>
              <p>
                {t('stock-p-1.p-3')}
              </p>

              <h3>{t('stock-h-2')}</h3>
              <p>
                {t('stock-p-2')}{' '} <Link to="/saleinfo/trade_comissions">{t('stock-link-1')}</Link>.
              </p>
              <h3>{t('stock-h-3')}</h3>
              <p>
                {t('stock-p-3')}
              </p>
              <h3>{t('stock-h-4')}</h3>
              <p>
                {t('stock-p-4.p-1')}
              </p>
              <p>
                {t('stock-p-4.p-2')}
              </p>
            </div>
            <HelpColumn/>
          </section>
        </div>
      </DocumentTitle>
    </Layout>
  )
}

export default withNamespaces()(Stock)
