// react
import React from 'react'
import Layout from '../../../../hocs/Layout/Layout'
import {t} from 'i18next'
// title info
import DocumentTitle from 'react-document-title'

// styles
import './Calendar.css'

import {withNamespaces} from 'react-i18next'
import {Link} from 'react-router-dom'
import arrow from '../../../../assets/innerPages/angle-arrow-down.png'
import HelpColumn from '../../HelpColumn/HelpColumn'

const Calendar = () => {
  return (
    <Layout>
      <DocumentTitle title={t('economic calendar')}>
        <div className="content-block">
          <div className="in-block-nav">
            <Link to="/">{t('main')}</Link>
            <img src={arrow} alt='' className={'arrow-right'}/>
            {t('economic calendar')}
          </div>
          <h1 className='block-header'>{t('economic calendar')}</h1>
          <section>
            <div className="left-col">
              <iframe
                title="calendar"
                frameBorder="0"
                // scrolling="no"
                src="https://sslecal2.forexprostools.com?columns=exc_flags,exc_currency,exc_importance,exc_actual,exc_forecast,exc_previous&features=datepicker,timezone&countries=25,4,17,39,72,26,10,6,37,43,56,36,5,61,22,12,35&calType=week&timeZone=18&lang=1"
                className="frame"
              />
            </div>
            <HelpColumn/>
          </section>

        </div>
      </DocumentTitle>
    </Layout>
  )
}

export default withNamespaces()(Calendar)
