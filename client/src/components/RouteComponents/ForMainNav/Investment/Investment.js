// react
import React from 'react'
import {Link} from 'react-router-dom'
import {withNamespaces} from 'react-i18next'

// title info
import DocumentTitle from 'react-document-title'
import {t} from 'i18next'

// hocs
import Layout from '../../../../hocs/Layout/Layout'

// components
import HelpColumn from '../../HelpColumn/HelpColumn'
import arrow from '../../../../assets/innerPages/angle-arrow-down.png'

const Investment = () => {
  return (
    <Layout>
      <DocumentTitle title={t('investment portfolios')}>
        <div className="content-block">
          <div className="in-block-nav">
            <Link to="/">{t('main')}</Link>
            <img src={arrow} alt='' className={'arrow-right'}/>
            {t('investment portfolios')}
          </div>
          <h1 className='invest-header'>{t('invest-h-1')}</h1>
          <section>
            <div className="left-col">
              <p>{t('invest-p-1')}</p>

              <h3>{t('invest-h-2')}</h3>
              <p>{t('invest-p-2')}</p>

              <p><span style={{'text-decoration': 'underline'}}>{t('invest-sub-1')}</span>{' '}{t('invest-sub-p-1')}</p>
              <p><span style={{'text-decoration': 'underline'}}>{t('invest-sub-2')}</span>{' '}{t('invest-sub-p-2')}</p>
              <p><span style={{'text-decoration': 'underline'}}>{t('invest-sub-3')}</span>{' '}{t('invest-sub-p-3')}</p>

              <h3>{t('invest-h-3')}</h3>
              <p>{t('invest-p-3')}</p>
              <p>{t('invest-p-3-1')}</p>

              <h3>{t('invest-h-4')}</h3>
              <li>{t('invest-sub-4')}</li>
              <li>{t('invest-sub-5')}</li>

            </div>
            <HelpColumn/>
          </section>
        </div>
      </DocumentTitle>
    </Layout>
  )
}

export default withNamespaces()(Investment)
