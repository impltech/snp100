// react
import React from 'react'
import {Link} from 'react-router-dom'
import {withNamespaces} from 'react-i18next'

// title info
import DocumentTitle from 'react-document-title'
import {t} from 'i18next'

// hocs
import Layout from '../../../../hocs/Layout/Layout'

// components
import HelpColumn from '../../HelpColumn/HelpColumn'
import arrow from '../../../../assets/innerPages/angle-arrow-down.png'

const CryptoCurrency = () => {
  return (
    <Layout>
      <DocumentTitle title={t('cryprocurrency-H')}>
        <div className="content-block">
          <div className="in-block-nav">
            <Link to="/">{t('main')}</Link>
            <img src={arrow} alt='' className={'arrow-right'}/>
            {t('cryprocurrency-H')}
          </div>
          <h1>{t('cryprocurrency-H')}</h1>
          <section>
            <div className="left-col">
              <h3>{t('cryprocurrency-h-1')}</h3>
              <p>{t('cryprocurrency-p-1.p-1')}</p>
              <p>{t('cryprocurrency-p-1.p-2')}</p>
              <b>{t('cryprocurrency-sub-h')}</b>

              <h3>{t('cryprocurrency-h-2')}</h3>
              <p>{t('cryprocurrency-p-2.p-1')}</p>
              <p>{t('cryprocurrency-p-2.p-2')}</p>

              <h3>{t('cryprocurrency-h-3')}</h3>
              <p>{t('cryprocurrency-p-3.p-1')}</p>
              <p>{t('cryprocurrency-p-3.p-2')}</p>

              <h3>{t('cryprocurrency-h-4')}</h3>
              <p>{t('cryprocurrency-p-4')}</p>

              <h3>{t('cryprocurrency-h-5')}</h3>
              <p>{t('cryprocurrency-p-5')}</p>

              <h3>{t('cryprocurrency-h-6')}</h3>
              <p>{t('cryprocurrency-p-6.p-1')}</p>
              <p>{t('cryprocurrency-p-6.p-2')}</p>

              <h3>{t('cryprocurrency-h-7')}</h3>
              <p>{t('cryprocurrency-p-7')}</p>
              <p>{t('cryprocurrency-p-8')}</p>

            </div>
            <HelpColumn/>
          </section>
        </div>
      </DocumentTitle>
    </Layout>
  )
}

export default withNamespaces()(CryptoCurrency)
