// react
import React from 'react'
import {Link} from 'react-router-dom'

// title info
import DocumentTitle from 'react-document-title'
import {t} from 'i18next'

// hocs
import Layout from '../../../../hocs/Layout/Layout'

// components
import HelpColumn from '../../HelpColumn/HelpColumn'

import {withNamespaces} from 'react-i18next'
import arrow from '../../../../assets/innerPages/angle-arrow-down.png'

const Debentures = () => {
  return (
    <Layout>
      <DocumentTitle title={t('debentures-H')}>
        <div className="content-block">
          <div className="in-block-nav">
            <Link to="/">{t('main')}</Link>
            <img src={arrow} alt='' className={'arrow-right'}/>
            {t('debentures-H')}
          </div>
          <h1 className='block-header'>{t('debentures-H')}</h1>
          <section>
            <div className="left-col">
              <h3>{t('debentures-h-1')}</h3>
              <p>
                {t('debentures-p-1')}
              </p>
              <div>
                <b>{t('debentures-sub-h-1')}</b>
                <ul>
                  <li>
                    {t('debentures-li-1')}
                  </li>
                  <li> {t('debentures-li-2')}</li>
                  <li> {t('debentures-li-3')}</li>
                </ul>
              </div>
              <p>
                {t('debentures-p-2')}
              </p>
              <h3>{t('debentures-h-2')}</h3>
              <div>
                <b>{t('debentures-sub-h-2')}</b>
                <ul>
                  <li>
                    <b>{t('debentures-li-4-sub-h')}</b>
                    {t('debentures-li-4')}
                  </li>
                  <li><b>{t('debentures-li-5-sub-h')}</b>{t('debentures-li-5')}</li>
                </ul>
                <p>
                  {t('debentures-p-3')}
                  <Link to="/saleinfo/trade_comissions">
                    {t('debentures-link-1')}
                  </Link>
                </p>
              </div>
              <h3>{t('debentures-h-3')}</h3>
              <p>
                {t('debentures-p-4')}
              </p>
              <h3>{t('debentures-h-4')}</h3>
              <p>
                {t('debentures-p-5.p-1')}
              </p>
              <p>
                {t('debentures-p-5.p-2')}
              </p>
            </div>
            <HelpColumn/>
          </section>
        </div>
      </DocumentTitle>
    </Layout>
  )
}

export default withNamespaces()(Debentures)
