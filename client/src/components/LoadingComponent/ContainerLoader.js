import React from 'react'
import './Loader.css'

const ContainerLoader = (isLoading, error) => {
  if (isLoading) {
    return (
      <div className="loader">
        <div className='container-loader'></div>
      </div>
    )
  } else if (error) {
    return <div className='error'></div>
  } else {
    return null
  }
}

export default ContainerLoader