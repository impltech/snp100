import React from 'react'
import './Loader.css'

const LoadingComponent = (isLoading, error) => {
  if (isLoading) {
    return (
      <div className="loader">
        <div>Loading...</div>
      </div>
    )
  } else if (error) {
    return <div>Sorry, there was a problem loading the page.</div>
  } else {
    return null
  }
}

export default LoadingComponent