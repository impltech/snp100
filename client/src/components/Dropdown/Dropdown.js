export function setRollUp () {
  const rolls = Array.from(document.getElementsByClassName('dropdown-paragraph'))
  rolls[0] && rolls.forEach((div) => {
    if (div.firstElementChild) div.style.height = div.firstElementChild.clientHeight + 'px'
  })
}

export function dropdown (event) {
  let div
  let headerH
  if (event.target.parentElement.className === 'dropdown-paragraph') {
    div = event.target.parentElement
  } else if (event.target.className === 'dropdown-paragraph') {
    div = event.target
  } else {
    return
  }
  headerH = div.firstElementChild ? div.firstElementChild.clientHeight : 0
  if (div.clientHeight > headerH) {
    div.style.height = headerH + 'px'
  } else {
    div.style.height = '100%'
  }
}