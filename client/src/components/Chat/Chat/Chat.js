import React, {Component, Fragment} from 'react'
import {withNamespaces} from 'react-i18next'
import {t} from 'i18next'

import consult from '../../../assets/materials/consultant.png'

class Chat extends Component {
  render () {
    const {expand, close, change, dialog, addToDialog, isSending, printing, send, onEnter} = this.props
    return (
      <div className="chat" id='chat'>
        <div className="chat-close" onClick={close}></div>
        <div className="chat-header" onClick={expand}>
          <div className="chat-img-container">
            <img src={consult} alt=""/>
          </div>
          <div className='chat-header-text'>
            <h5>{t('personal-analyst')}</h5>
            <p>{t('answer-for-question')}</p>
          </div>
        </div>
        <div className="chat-wrapper">
          <div className="chat-chat-container" id='chat-wrap'>
            {dialog[0] &&
            dialog.map(d =>
              <div className={d.speaker === 'bot' ? 'bot-speech' : 'user-speech'}>
                <p>{d.speech}</p>
                {d.contactRequestForm &&
                <div className={'contact-form'} >
                  <input type="text"
                    name='phone'
                    placeholder={t('chat-phone-field')}
                    className='phone-field'
                    onChange={change}/>
                  <input type="text"
                    name='name'
                    placeholder={t('chat-name')}
                    className='name-field'
                    onChange={change}/>
                  <button className={'send-btn'}
                    onClick={send}>{t('chat-send')}
                  </button>
                </div>}
                <div className="sending-time">
                  {t('sending-time')}{' '}{d.time}
                </div>
              </div>
            )

            }

          </div>
          <div className="printing-indicator-container">
            {printing &&
            <div className="printing-indicator">
              <p>{t('wait-for-a-message')}</p>
              <div className="dot-typing"></div>
            </div>}
          </div>
          {!isSending &&
          <Fragment>
            <textarea className={'speech-input'}
              name={'speech'}
              onChange={change} onKeyUp={onEnter}>
            </textarea>
            <button className="send-speech" onClick={addToDialog}></button>
          </Fragment>
          }
        </div>

      </div>

    )
  }
}

export default withNamespaces()(Chat)