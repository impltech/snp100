import React, {Component} from 'react'
import './Chat.css'
import Chat from './Chat/Chat'
import CallRequest from './CallRequest/CallRequest'
import {callRequest} from '../../const/ApiSetting'
import {t} from 'i18next'
import {withNamespaces} from 'react-i18next'

class ChatBox extends Component {
  constructor (props) {
    super(props)
    this.state = {
      chatOpen: false,
      callReqSending: false,
      date: new Date().toLocaleDateString(),
      time: new Date().toLocaleTimeString(),
      speech: '',
      speechSending: false,
      printing: false,
      dialog: []
    }

    this.handleChange = this.handleChange.bind(this)
    this.callReqSend = this.callReqSend.bind(this)
    this.closeChatBox = this.closeChatBox.bind(this)
    this.expandCallReq = this.expandCallReq.bind(this)
    this.expandChat = this.expandChat.bind(this)
    this.dialogInit = this.dialogInit.bind(this)
    this.addToDialog = this.addToDialog.bind(this)
    this.goToChat = this.goToChat.bind(this)
    this.handleEnter = this.handleEnter.bind(this)
  }

  handleChange (event) {
    const {name, value} = event.target
    this.setState({[name]: value})
  }

  handleEnter (event) {
    if (event.keyCode === 13) {
      this.addToDialog()
    }
  }

  callReqSend () {
    const url = callRequest
    let query = {
      sendingTime: new Date().toLocaleString(),
      name: this.state.name,
      phone: this.state.phone,
      date: this.state.date,
      time: this.state.time,
      speech: this.state.speech
    }
    let reqParam = {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify(query)
    }
    if (this.state.name && this.state.phone) {
      fetch(url, reqParam)
        .then(res => res.json())
        .then(this.setState({callReqSending: true}))
        .then(this.finalMessage())
    }
  }

  addToDialog () {
    const time = new Date().toLocaleTimeString()
    const speech = {
      speaker: 'user',
      speech: this.state.speech,
      time: time
    }
    if (this.state.speech.trim()) {
      this.state.dialog.push(speech)
      this.setState({speechSending: true})
      this.answerToMessege()
    }
  }

  dialogInit () {
    const firstSpeech = {
      speaker: 'bot',
      speech: t('chat-hello-1'),
      time: new Date().toLocaleTimeString()
    }
    this.setState({printing: true})
    setTimeout(() => {
      this.state.dialog.push(firstSpeech)
      this.setState({printing: true})
    }, 1000)
    setTimeout(() => {
      this.state.dialog.push(
        {
          speaker: 'bot',
          speech: t('chat-hello-2'),
          time: new Date().toLocaleTimeString()
        }
      )
      this.setState({printing: false})
    }, 5000)
  }

  answerToMessege () {
    this.setState({printing: true})
    setTimeout(() => {
      this.state.dialog.push(
        {
          speaker: 'bot',
          speech: t('contact-request'),
          time: new Date().toLocaleTimeString(),
          contactRequestForm: true
        }
      )
      this.setState({printing: false})
      this.scrollDown()
    }, 4000)
  }

  finalMessage () {
    this.setState({printing: true})
    setTimeout(() => {
      this.state.dialog.push(
        {
          speaker: 'bot',
          speech: t('chat-thanks-1'),
          time: new Date().toLocaleTimeString()
        }
      )
      this.setState({printing: false})
      this.scrollDown()
    }, 3000)
  }

  scrollDown () {
    const el = document.getElementById('chat-wrap')
    if (el) {
      console.log('scroll!')
      el.scrollTop = el.scrollHeight
    }
  }

  rollup () {
    const chatbox = document.getElementById('chat-box')
    const chat = document.getElementById('chat')
    const callReq = document.getElementById('call-request')
    if (!this.state.chatOpen && chatbox && chat) {
      callReq.className = 'call-request-rollup'
      chatbox.className = 'chat-box chat-box-rollup'
    }
  }

  minimize () {
    const chatbox = document.getElementById('chat-box')
    const chat = document.getElementById('chat')
    const callReq = document.getElementById('call-request')
    if (!this.state.chatOpen && chatbox && chat) {
      callReq.className = 'call-request-hidden'
      chatbox.className = 'chat-box chat-box-hidden'
      chat.className = 'chat'
    }
  }

  expandCallReq () {
    const chatbox = document.getElementById('chat-box')
    const chat = document.getElementById('chat')
    const callReq = document.getElementById('call-request')
    this.setState({chatOpen: true})
    if (chatbox && chat && callReq) {
      callReq.className = 'call-request-expanded'
      chatbox.className = 'chat-box chat-expanded'
      chat.className = 'chat-hidden'
      chatbox.onmouseleave = null
    }
  }

  expandChat () {
    const chatbox = document.getElementById('chat-box')
    const chat = document.getElementById('chat')
    const callReq = document.getElementById('call-request')
    this.setState({chatOpen: true})
    if (chatbox && chat && callReq) {
      callReq.className = 'call-request-hidden'
      chatbox.className = 'chat-box chat-expanded'
      chat.className = 'chat-chat-expanded'
      chatbox.onmouseleave = null
      if (!this.state.dialog[0]) this.dialogInit()
    }
  }

  goToChat () {
    this.closeChatBox()
    this.expandChat()
  }

  closeChatBox () {
    this.setState({chatOpen: false})
    this.minimize()
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    if (prevState.chatOpen !== this.state.chatOpen && prevState.chatOpen && !this.state.chatOpen) {
      this.closeChatBox()
    }
  }

  render () {
    return (
      <div className="chat-container">
        <div className="chat-box chat-box-hidden" id='chat-box'
          onMouseEnter={this.rollup.bind(this)} onMouseLeave={this.minimize.bind(this)}>
          <Chat expand={this.expandChat}
            close={this.closeChatBox}
            change={this.handleChange}
            dialog={this.state.dialog}
            addToDialog={this.addToDialog}
            isSending={this.state.speechSending}
            init={this.dialogInit}
            printing={this.state.printing}
            send={this.callReqSend}
            onEnter={this.handleEnter}
          />
          <CallRequest expand={this.expandCallReq}
            close={this.closeChatBox}
            change={this.handleChange}
            send={this.callReqSend}
            isSending={this.state.callReqSending}
            toChat={this.goToChat}/>
        </div>
      </div>

    )
  }
}

export default withNamespaces()(ChatBox)