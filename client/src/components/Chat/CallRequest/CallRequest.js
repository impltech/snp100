import React, {Component} from 'react'
import {withNamespaces} from 'react-i18next'
import {t} from 'i18next'
import phone from '../../../assets/materials/Phone.png'

class CallRequest extends Component {
  constructor (props) {
    super(props)
    this.state = {
      callReqSending: false
    }
  }

  render () {
    const {change, send, expand, isSending, toChat} = this.props

    return (
      <div className="call-request-hidden" id='call-request'>
        <div className="chat-close" onClick={this.props.close}></div>
        <div className="chat-header" onClick={expand}>
          <div className="chat-img-container">
            <img src={phone} alt=""/>
          </div>
          <div className='chat-header-text'>
            <h5>{t('call-request')}</h5>
            <p>{t('we-recall')}</p>
          </div>
        </div>
        {!isSending
          ? <div className="call-req-form">
            <p>
              {t('choose-time')}
            </p>

            <div className="date-time-fields">
              <select name={'date'} onChange={change}>
                <option value={'сегодня'}>{t('today')}</option>
                <option value={'завтра'}>{t('tomorrow')}</option>
              </select>
              <select name={'time'} onChange={change}>
                <option value="now">{t('now')}</option>
                <option value="7.00">7.00-8.00</option>
                <option value="8.00">8.00-9.00</option>
                <option value="9.00">9.00-10.00</option>
                <option value="10.00">10.00-11.00</option>
                <option value="11.00">11.00-12.00</option>
                <option value="12.00">12.00-13.00</option>
                <option value="13.00">13.00-14.00</option>
                <option value="14.00">14.00-15.00</option>
                <option value="15.00">15.00-16.00</option>
                <option value="16.00">16.00-17.00</option>
                <option value="17.00">17.00-18.00</option>
                <option value="18.00">18.00-19.00</option>
                <option value="19.00">19.00-20.00</option>
              </select>
            </div>
            <input type="text"
              name='phone'
              placeholder={t('chat-phone-field')}
              className='phone-field'
              onChange={change}/>
            <input type="text"
              name='name'
              placeholder={t('chat-name')}
              className='name-field'
              onChange={change}/>
            <button className={'send-btn'}
              onClick={send}>{t('wait-for-call')}
            </button>
            <div>
              <p className={'return-to-chat'} onClick={toChat}>{t('return-to-chat')}</p>
            </div>
          </div>
          : <div className="call-req-form">
            <p>{t('chat-thanks-2')}</p>
          </div>
        }
      </div>

    )
  }
}

export default withNamespaces()(CallRequest)