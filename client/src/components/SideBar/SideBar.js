// react
import React, {Component} from 'react'
import {Link} from 'react-router-dom'

// translation
import { withNamespaces } from 'react-i18next'
import i18next from 'i18next'

// semantic ui
import {Image} from 'semantic-ui-react'

// styles
import './SideBar.css'

// logo
import lang from '../../assets/materials/language_icon.png'
import register from '../../assets/materials/reg_icon.png'
import login from '../../assets/materials/login_icon.png'
import {cabinet, registration, webTrader} from '../../const/ApiSetting'

const logoStyle = {
  width: '20px',
  height: '20px',
  marginRight: '10px'
}

class SideBar extends Component {
  state = {
    isShowMenu: false
  }

  toggleSidebarMenu = () => {
    this.setState({isShowMenu: !this.state.isShowMenu})
  }

  render () {
    const hidden = this.props.hidden ? 'sidebar' : 'sidebar-hidden'
    const showSidebarMenu = this.state.isShowMenu
      ? 'sidebar-study-center'
      : 'sidebar-study-center-hide'
    return (
      <div className={hidden}>
        <ul className="sidebar-list">
          <li>
            <div className="sidebar-link">
              <Link to="/bonus">{i18next.t('bonuses')}</Link>
            </div>
          </li>
          <li>
            <div className="sidebar-link">
              <Link to="/saleinfo/diff_contracts">{i18next.t('trading info')}</Link>
            </div>
          </li>
          <li>
            <div className="sidebar-link">
              <Link to="/faq/disputes">FAQ</Link>
            </div>
          </li>
          <li>
            <div className="sidebar-link">
              <Link to="/contacts">{i18next.t('contacts')}</Link>
            </div>
          </li>
          <li>
            <div className="sidebar-link">
              <Link to="/accounts">{i18next.t('accounts-types')}</Link>
            </div>
          </li>
          <li>
            <div className="sidebar-link">
              <a href={webTrader}>WEBTRADER</a>
            </div>
          </li>
        </ul>
        <ul className="sidebar-list">
          <li onClick={this.toggleSidebarMenu} className="sidebar-sidebar-study">
            <div className="sidebar-link">
              {i18next.t('learning center')}
            </div>
            <ul className={showSidebarMenu} onClick={this.toggleSidebarMenu}>
              <Link to="/stock" className="sidebar-study-links">
                <span>{i18next.t('stocks')}</span>
              </Link>
              <Link to="/indexes" className="sidebar-study-links">
                <span>{i18next.t('indexes')}</span>
              </Link>
              <Link to="/products" className="sidebar-study-links">
                <span>{i18next.t('commodity')}</span>
              </Link>
              <Link to="/forex" className="sidebar-study-links">
                <span>{i18next.t('forex')}</span>
              </Link>
              <Link to="/cfd" className="sidebar-study-links">
                <span>CFD</span>
              </Link>
            </ul>
          </li>
          <li>
            <div className="sidebar-link">
              <Link style={{color: 'white'}} to="/investment">
                {i18next.t('investment portfolios')}
              </Link>
            </div>
          </li>
          <li>
            <div className="sidebar-link">
              <Link style={{color: 'white'}} to="/debentures">
                {i18next.t('debentures')}
              </Link>
            </div>
          </li>
          <li>
            <div className="sidebar-link">
              <Link style={{color: 'white'}} to="/calendar">
                {i18next.t('economic calendar')}
              </Link>
            </div>
          </li>
        </ul>
        <ul className="sidebar-list">
          <ul className="sidebar-register">
            <li>
              <Image src={login} alt="login_logo" style={logoStyle}/>
              <div className="sidebar-link">
                <a href={cabinet}>{i18next.t('sing in')}</a>
              </div>
            </li>
            <li>
              <Image src={register} alt="register_logo" style={logoStyle}/>
              <div className="sidebar-link">
                <a href={registration}>{i18next.t('registration')}</a>
              </div>
            </li>
          </ul>
          <li className="lang-logo">
            <Image
              src={lang}
              alt="lang_logo"
              onClick={this.props.modal_open}
              style={logoStyle}
            />
          </li>
        </ul>
      </div>
    )
  }
}

export default withNamespaces()(SideBar)
