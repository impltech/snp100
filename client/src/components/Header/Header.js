// react
import React from 'react'

// styles
import './Header.css'

// component
import UpperNav from './UpperNav/UpperNav'
import MainNav from './MainNav/MainNav'

const Header = props => {
  return (
    <div className="header" onClick={props.hide}>
      <UpperNav modal_open={props.modal_open} />
      <MainNav
        show={props.show}
        menuButtonIsClicked={props.menuButtonIsClicked}
        isRotated={props.isRotated}
      />
    </div>
  )
}
export default Header
