// react
import React from 'react'
import {Link} from 'react-router-dom'

// semantic ui
import {Image} from 'semantic-ui-react'

// styles
import './UpperNav.css'

// logo
import lang from '../../../assets/materials/language_icon.png'
import register from '../../../assets/materials/reg_icon.png'
import login from '../../../assets/materials/login_icon.png'

// the hoc
import {withNamespaces} from 'react-i18next'
import i18next from 'i18next'
import {cabinet, registration, webTrader} from '../../../const/ApiSetting'

const logoStyle = {
  width: '25px',
  height: '25px',
  marginRight: '10px'
}

const langLogoStyle = {
  height: '24px',
  width: '24px',
  margin: 0
}

const UpperNav = (props) => {
  return (
    <div className="upper-nav">
      <nav className="upper-menu-block">
        <div className="left-menu-block">
          <ul className="lang">
            <li>
              <Image
                src={lang}
                alt="lang_logo"
                style={langLogoStyle}
                onClick={props.modal_open}
                className='lang-link'
              />
            </li>
          </ul>

          <ul className="left-list">
            <li>
              <Link to="/bonus">{i18next.t('bonuses')}</Link>
            </li>
            <li>
              <Link to="/saleinfo/diff_contracts">{i18next.t('trading info')}</Link>
            </li>
            <li>
              <Link to="/faq/disputes">FAQ</Link>
            </li>
            <li>
              <Link to="/contacts">{i18next.t('contacts')}</Link>
            </li>
            <li>
              <Link to="/accounts">{i18next.t('accounts-types')}</Link>
            </li>
          </ul>
        </div>

        <div className="right-menu-block">
          <ul className="right-list">
            <li>
              <a href={webTrader}>WEBTRADER</a>
            </li>
            <li>
              <Image src={login} alt="login_logo" style={logoStyle}/>
              <a href={cabinet}>{i18next.t('sing in')}</a>
            </li>
            <li>
              <Image src={register} alt="register_logo" style={logoStyle}/>
              <a href={registration}>{i18next.t('registration')}</a>
            </li>
          </ul>
        </div>

      </nav>
    </div>
  )
}

export default withNamespaces()(UpperNav)
