// react
import React, {Component} from 'react'
import {Link} from 'react-router-dom'

// semantic ui
import {Image} from 'semantic-ui-react'
import logo from '../../../assets/materials/logo.png'

// styles
import './MainNav.css'

// translation
import {withNamespaces} from 'react-i18next'
import i18next from 'i18next'

class MainNav extends Component {
  state = {
    isShowMenu: false
  }

  toggleMenu = () => {
    this.setState({isShowMenu: !this.state.isShowMenu})
  }

  menuButtonIsClicked = () => {
    this.setState({
      isRotated: !this.state.isRotated
    })
  }

  render () {
    const showMenu = this.state.isShowMenu
      ? 'study-center'
      : 'study-center-hide'
    return (
      <div className="main-nav">
        <nav className="main-menu-block">
          <div className="logo-block">
            <ul>
              <li>
                <div className="logo-img">
                  <Link to='/'><Image alt="logo" src={logo}/></Link>
                </div>
              </li>
            </ul>
          </div>
          <div className="main-menu">
            <ul>
              <li onClick={this.toggleMenu} className="study">
                <a href="#">{i18next.t('learning center')}</a>
              </li>

              <div className={showMenu} onClick={this.toggleMenu}>
                <Link to="/stock" className="study-links">
                  <span>{i18next.t('stocks')}</span>
                </Link>
                <Link to="/indexes" className="study-links">
                  <span>{i18next.t('indexes')}</span>
                </Link>
                <Link to="/products" className="study-links">
                  <span>{i18next.t('commodity')}</span>
                </Link>
                <Link to="/forex" className="study-links">
                  <span>{i18next.t('forex')}</span>
                </Link>
                <Link to="/cfd" className="study-links">
                  <span>CFD</span>
                </Link>
              </div>
              <li>
                <Link to="/investment">{i18next.t('investment portfolios')}</Link>
              </li>
              <li>
                <Link to="/debentures">{i18next.t('debentures')}</Link>
              </li>
              <li>
                <Link to="/calendar">{i18next.t('economic calendar')}</Link>
              </li>
            </ul>
          </div>
        </nav>
        <div className="cont" onClick={this.props.menuButtonIsClicked}>
          <div onClick={this.props.show}>
            <div className={this.props.isRotated ? 'change-bar1' : 'bar1'}/>
            <div className={this.props.isRotated ? 'change-bar2' : 'bar2'}/>
            <div className={this.props.isRotated ? 'change-bar3' : 'bar3'}/>
          </div>
        </div>
      </div>
    )
  }
}

export default withNamespaces()(MainNav)
