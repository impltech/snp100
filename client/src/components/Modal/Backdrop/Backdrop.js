// react
import React from 'react'

// styles
import './Backdrop.css'

const Backdrop = props => {
  return props.open ? (
    <div className="backdrop" onClick={props.modal_closed} />
  ) : null
}

export default Backdrop
