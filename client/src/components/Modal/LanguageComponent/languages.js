const languages = [
  {
    title: 'english',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Flag_of_Great_Britain_%281707%E2%80%931800%29.svg/1280px-Flag_of_Great_Britain_%281707%E2%80%931800%29.svg.png',
    locale: 'en'
  },
  {
    title: 'العربية',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Flag_of_Saudi_Arabia.svg/1280px-Flag_of_Saudi_Arabia.svg.png',
    locale: 'ar'
  },
  {
    title: 'български',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Flag_of_Bulgaria.svg/1280px-Flag_of_Bulgaria.svg.png',
    locale: 'bg'
  },
  {
    title: 'ελληνικά',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_Greece.svg/2000px-Flag_of_Greece.svg.png',
    locale: 'ka'
  },
  {
    title: 'ქართული',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/b/bb/Flag_of_Georgia_official.svg',
    locale: 'da'
  },
  {
    title: 'dansk',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/Flag_of_Denmark.svg/740px-Flag_of_Denmark.svg.png',
    locale: 'de'
  },
  {
    title: 'עברית',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Flag_of_Israel.svg/1280px-Flag_of_Israel.svg.png',
    locale: 'el'
  },
  {
    title: 'español',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Flag_of_Spain.svg/800px-Flag_of_Spain.svg.png',
    locale: 'es'
  },
  {
    title: 'italiano',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Flag_of_Italy.svg/800px-Flag_of_Italy.svg.png',
    locale: 'it'
  },
  {
    title: '中文',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/2/2e/Flag_of_China.png',
    locale: 'cs'
  },
  {
    title: '한국어',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/0/0f/Flag_of_South_Korea.png',
    locale: 'ko'
  },
  {
    title: 'latviešu',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Flag_of_Latvia.svg/1280px-Flag_of_Latvia.svg.png',
    locale: 'lv'
  },
  {
    title: 'Lietuvos',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Flag_of_Lithuania.svg/1280px-Flag_of_Lithuania.svg.png',
    locale: 'lt'
  },
  {
    title: 'Deutsch',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Flag_of_Germany.svg/1024px-Flag_of_Germany.svg.png',
    locale: 'he'
  },
  {
    title: 'norsk',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Flag_of_Norway%2C_state.svg/1280px-Flag_of_Norway%2C_state.svg.png',
    locale: 'no'
  },
  {
    title: 'polski',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/a/aa/Poland_flag_300.png',
    locale: 'pl'
  },
  {
    title: 'Türk',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/b/bb/Turkey_flag_300.png',
    locale: 'tr'
  },
  {
    title: 'français',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/6/62/Flag_of_France.png',
    locale: 'fr'
  },
  {
    title: 'हिन्दी',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/b/bc/Flag_of_India.png',
    locale: 'hi'
  },
  {
    title: 'český',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cb/Flag_of_the_Czech_Republic.svg/1280px-Flag_of_the_Czech_Republic.svg.png',
    locale: 'zh'
  },
  {
    title: 'svenska',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Flag_of_Sweden.svg/800px-Flag_of_Sweden.svg.png',
    locale: 'sv'
  },
  {
    title: 'eesti',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Flag_of_Estonia.svg/1280px-Flag_of_Estonia.svg.png',
    locale: 'et'
  },
  {
    title: '日本語',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Flag_of_Japan.svg/1280px-Flag_of_Japan.svg.png',
    locale: 'ja'
  },
  {
    title: 'Australian',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/8/88/Flag_of_Australia_%28converted%29.svg',
    locale: 'uk'
  },
  {
    title: 'Русский',
    flag:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Flag_of_Russia.svg/800px-Flag_of_Russia.svg.png',
    locale: 'ru'
  }
]

export default languages
