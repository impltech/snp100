// react
import React from 'react'

// styles
import './LanguageComponent.css'

// data
import languages from './languages'

// components
import Languages from './Languages/Languages'

import i18n from '../../../i18n'
import { withNamespaces } from 'react-i18next'

const LanguageComponent = ({ t }) => {
  const changeLanguage = lng => {
    i18n.changeLanguage(lng)
    console.log('change', lng)
  }
  return (
    <div className="language">
      <h2>{t('choose language')}</h2>
      <div className="language-items">
        <Languages languages={languages} change={changeLanguage} />
      </div>
    </div>
  )
}

export default withNamespaces()(LanguageComponent)
