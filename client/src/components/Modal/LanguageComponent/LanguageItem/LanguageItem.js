// react
import React from 'react'

// semantic ui
import { Image } from 'semantic-ui-react'

// styles
import '../LanguageComponent.css'

const imgStyle = {
  width: '30px',
  height: '20px'
}

const LanguageItem = props => {
  return (
    <div className="flag_item" onClick={() => props.change(props.locale)}>
      <Image alt="" src={props.flag} style={imgStyle} />
      &nbsp;&nbsp;&nbsp;
      <span>{props.title}</span>
    </div>
  )
}

export default LanguageItem
