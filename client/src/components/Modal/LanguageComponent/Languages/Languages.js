// react
import React from 'react'

// components
import LanguageItem from '../LanguageItem/LanguageItem'

// styles
import '../LanguageComponent.css'

const Languages = props => {
  return (
    <div className="language-col">
      {props.languages.map(item => {
        return (
          <LanguageItem
            key={item.flag}
            title={item.title}
            flag={item.flag}
            locale={item.locale}
            change={props.change}
          />
        )
      })}
    </div>
  )
}

export default Languages
