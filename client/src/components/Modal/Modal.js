// react
import React from 'react'

// components
import Backdrop from './Backdrop/Backdrop'

// styles
import './Modal.css'

const Modal = props => {
  return (
    <div className="modal-block">
      <Backdrop open={props.open} modal_closed={props.modal_closed} />
      <div
        className="modal"
        style={{
          transform: props.open ? 'translateY(0)' : 'translateY(-100vh)',
          opacity: props.open ? '1' : '0'
        }}
        onClick={props.modal_closed}
      >
        {props.children}
      </div>
    </div>
  )
}

export default Modal
