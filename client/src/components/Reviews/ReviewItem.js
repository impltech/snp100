import React from 'react'

const ReviewItem = props => {
  const text = props.text || ''
  const photo = props.photo || ''
  const name = props.name || 'unknown'

  function onmousedownHdl (e) {
    if (window.event && window.event.stopPropagation) {
      window.event.stopPropagation()
      window.event.cancelBubble = true
      e.cancelBubble = true
    }
  }

  return (
    <div>
      <div className="review-item">
        <div className="reviewer-photo-container">
          <img src={photo} alt="" className="reviewer-photo" onMouseDown={onmousedownHdl.bind(this)} draggable={false}/>
        </div>
        <div>
          <div className="review-text">
            <h2>{name}</h2>
            <p>{text}</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ReviewItem