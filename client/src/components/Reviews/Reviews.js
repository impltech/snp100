import React, {Component} from 'react'
import './Reviews.css'
import {t} from 'i18next'
import ReviewItem from './ReviewItem'
import {Carousel} from '../Carousel/carousel'

// == Photos ==
import avatar1 from '../../assets/reviews/trcom_icons/avatar1.jpg'
import avatar2 from '../../assets/reviews/trcom_icons/avatar2.jpg'
import avatar3 from '../../assets/reviews/trcom_icons/avatar3.jpg'
import avatar4 from '../../assets/reviews/trcom_icons/avatar4.jpg'
import avatar5 from '../../assets/reviews/trcom_icons/avatar5.jpg'
import avatar6 from '../../assets/reviews/trcom_icons/avatar6.jpg'
import avatar7 from '../../assets/reviews/trcom_icons/avatar7.jpg'
import avatar8 from '../../assets/reviews/trcom_icons/avatar8.jpg'
import avatar9 from '../../assets/reviews/trcom_icons/avatar9.jpg'
import avatar10 from '../../assets/reviews/trcom_icons/avatar10.jpg'
import {withNamespaces} from 'react-i18next'

class Reviews extends Component {
  render () {
    const reviews = {
      'review1': 'S&P100 предлагает реально хорошие условия для торговли, даже есть обучающий курс. Котировки обновляются с нормальной переодичностью, а не так как на некоторых сервисах, да и значения актуальные. С ними можно работать, планирую выводить сегодня первый профит.',
      'review2': 'Отличный интерфейс, огромный выбор индикаторов по техническому анализу, возможность использования автоматической торговли, правильное отношение к клиенту и многое другое я нашла у S&P100.',
      'review3': 'Однозначно лучший брокер, работаю совсем недавно, до этого бьл MT4, решил попробовать новое, получил первый профит, нареканий нет, все отлично.',
      'review4': 'Хорошие ребята, довольна тем, как они работают. Пока упреков и замечаний не имею. С точностью исполнения сделок все более чем хорошо, так что пока осталась у них.',
      'review5': 'Давно искал хорошего брокера. Вышел на S&P100 через знакомых: очень понравился и сервис, и отношение менеджеров. Надеюсь на дальнейшее сотрудничество.',
      'review6': 'Вначале думал очередная кухня, но коллега так рекомендовал, что решил сделать небольшой депозит. Получилось вывести первый профит достаточно быстро в сравнении с другими сервисами. Ориентированы на клиента, что не может не радовать. Работаю с S&P100, брокера менять пока не планирую.',
      'review7': 'Отличный сервис, S&P100 надежная контора, даже котировки вполне сносно грузят. Не очень понравилось обучение, но здесь нет ничего удивительно. Я нигде еще не видел, чтобы в каком-нибудь казино открыли обучающий курс «Как обыграть казино».',
      'review8': 'Вполне достойный трейдер, службы работают без нареканий, поддержку в рабочее время можно получить спокойно. До этого работал с другим сервисом, этот, как по мне, на голову выше.',
      'review9': 'Огромный выбор инструментов, наличие необходимых индексов и котировок, отличное обновление значений и тд. Много сервисов перепробывал, везде были какие-то проблемы. С S&P100 все четко на сколько это возможно.',
      'review10': 'Первый раз начал торговать лет 5 назад, тогда работал с MetaTrader 4 и зарабатывал, со временем пришел к выводу что хочу сменить платформу, 2-й месяц работаю на этом ресурсе, вполне доволен, без кидалова, а это главное.'
    }
    let slides = [
      <ReviewItem photo={avatar1} name='Ольга Павлова' text={reviews.review1}/>,
      <ReviewItem photo={avatar5} name='Сергей Рудкин' text={reviews.review5}/>,
      <ReviewItem photo={avatar6} name='Абзал Каримов' text={reviews.review6}/>,
      <ReviewItem photo={avatar4} name='Степан Потехин' text={reviews.review3}/>,
      <ReviewItem photo={avatar7} name='Роман Юдин' text={reviews.review7}/>,
      <ReviewItem photo={avatar2} name='Елена Новикова' text={reviews.review2}/>,
      <ReviewItem photo={avatar8} name='Антон Романов' text={reviews.review8}/>,
      <ReviewItem photo={avatar3} name='Татьяна Лебедева' text={reviews.review4}/>,
      <ReviewItem photo={avatar9} name='Дмитрий Пахмутов' text={reviews.review9}/>,
      <ReviewItem photo={avatar10} name='Владислав Морозов' text={reviews.review10}/>
    ]
    return (
      <section className='reviews-section'>
        <div className='reviews-container'>
          <Carousel slides={slides}/>
        </div>
      </section>
    )
  }
}

export default withNamespaces()(Reviews)