import React from 'react'
import {t} from 'i18next'
import {withNamespaces} from 'react-i18next'
import './CryptoSection.css'
import {Link} from 'react-router-dom'

const Crypto = () => {
  return (
    <div className="crypto-main-section">
      <div className="crypto__wrap-text">
        <h1>{t('crypto-banner-h-1')} {' '}<Link
          to="/crypto">{t('crypto-banner-h-2')}</Link> {' '}{t('crypto-banner-h-3')}</h1>

        <p>{t('crypto-banner-p')}</p>
      </div>

      <div className="coin coin-b">
        <div className="coin-body-outer b">
          <Link to='/crypto'>
            <div className="coin-body-inner inner1"></div>
          </Link>
        </div>
      </div>

      <div className="coin coin-l">
        <div className="coin-body-outer l">
          <Link to='/crypto'>
            <div className="coin-body-inner inner2"></div>
          </Link>
        </div>
      </div>

      <div className="coin coin-e">
        <div className="coin-body-outer e">
          <Link to='/crypto'>
            <div className="coin-body-inner inner3"></div>
          </Link>
        </div>
      </div>

      <div className="coin coin-r">
        <div className="coin-body-outer r">
          <Link to='/crypto'>
            <div className="coin-body-inner inner4"></div>
          </Link>
        </div>
      </div>

      <div className="coin coin-d">
        <div className="coin-body-outer d">
          <Link to='/crypto'>
            <div className="coin-body-inner inner5"></div>
          </Link>
        </div>
      </div>

      <div className="coin coin-eos">
        <div className="coin-body-outer eos">
          <Link to='/crypto'>
            <div className="coin-body-inner inner1"></div>
          </Link>
        </div>
      </div>

      <div className="line line1"></div>
      <div className="line line2"></div>
      <div className="line line3"></div>
      <div className="line line4"></div>
      <div className="line line5"></div>
      <div className="line line6"></div>
      <div className="line line7"></div>
    </div>
  )
}

export default withNamespaces()(Crypto)