// react
import React from 'react'

// components
import MarqueeItem from '../MarqueeItem/MarqueeItem'

// styles
import './Marquee.css'

const Marquee = props => {
  return (
    <div className="marquee-block">
      <div className="marquee">
        {props.quotes.map(item => {
          return (
            <MarqueeItem
              key={item.bid}
              ask={item.ask}
              bid={item.bid}
              symbol={item.symbol}
              price={item.price}
              timestamp={item.timestamp}
              delta = {item.delta}
              deltaAbs = {item.deltaAbs}
            />
          )
        })}
      </div>
    </div>
  )
}

export default Marquee
