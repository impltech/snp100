import React, {Component} from 'react'
import {connect} from 'react-redux'
import './StocksPanel.css'

class StocksPanel extends Component {
  render () {
    const {stocks} = this.props

    function grow (delta) {
      if (delta >= 0) {
        return 'marquee-growing'
      } else {
        return 'marquee-falling'
      }
    }

    return (
      (stocks[0] && stocks[0].symbol) ?

        <section className="stock-section">
          <div className='stocks-panel'>
            {stocks[0] && stocks.map(stock =>
              <div className="stock-item">
                <span className="stock-name">{stock.companyName}</span> (
                <span className="stock-symbol">{stock.symbol}): </span>{' '}
                <div className={grow(stock.changePercent) + ' price-block'}>
                  <span className="stock-price">{stock.latestPrice}</span>
                  <span>{' (' + stock.changePercent.toFixed(4) + '%)'}</span>
                </div>
              </div>
            )
            }
          </div>
        </section>

        : <div></div>
    )
  }
}

const mapStateToProps = state => ({
  stocks: state.stocksReducer.stocks
})

export default connect(mapStateToProps)(StocksPanel)