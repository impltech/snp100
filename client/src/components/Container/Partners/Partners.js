import React, {Component} from 'react'
import Slider from 'react-slick'

import './Partners.css'

import apple from '../../../assets/partners/icons/Apple.png'
import BOA from '../../../assets/partners/icons/bank-of-america.png'
import boing from '../../../assets/partners/icons/boeing.png'
import facebook from '../../../assets/partners/icons/facebook.png'
import google from '../../../assets/partners/icons/google.png'
import IBM from '../../../assets/partners/icons/international-business-machines.png'
import JPM from '../../../assets/partners/icons/JPMorgan.png'
import lamborg from '../../../assets/partners/icons/lamborghini.png'
import LinkedIn from '../../../assets/partners/icons/LinkedIn.png'
import mastercard from '../../../assets/partners/icons/Mastercard.png'
import mersedes from '../../../assets/partners/icons/Mercedes-Benz.png'
import PayPal from '../../../assets/partners/icons/PayPal.png'
import PM from '../../../assets/partners/icons/philip-morris.png'
import porsche from '../../../assets/partners/icons/Porsche_logo.png'
import TA from '../../../assets/partners/icons/transstates_airlines.png'
import visa from '../../../assets/partners/icons/visa.png'
import {withNamespaces} from 'react-i18next'

class Partners extends Component {
  render () {
    let mobileView = window.screen.availWidth < 480

    let settings = mobileView ? {
      dots: false,
      infinite: true,
      speed: 1000,
      slidesToShow: 3,
      slidesToScroll: 1,
      focusOnSelect: true
    } : {
      dots: false,
      infinite: true,
      speed: 1000,
      slidesToShow: 7,
      slidesToScroll: 4,
      focusOnSelect: true
    }
    return (
      <section className='partners'>
        <div className="partners-container">
          <div className="partners-header">
          </div>

          <div className="signs-container">
            <Slider {...settings}>
              <div>
                <div className='partner-sign'>
                  <img alt='' src={BOA}/>
                </div>
              </div>
              <div>
                <div className='partner-sign'>
                  <img src={apple} alt="apple"/>
                </div>
              </div>
              <div>
                <div className='partner-sign'>
                  <img src={boing} alt=''/>
                </div>
              </div>

              <div>
                <div className='partner-sign'>
                  <img src={google} alt=''/>
                </div>
              </div>
              <div>
                <div className='partner-sign'>
                  <img src={IBM} alt=''/>
                </div>
              </div>
              <div>
                <div className='partner-sign'>
                  <img src={facebook} alt=''/>
                </div>
              </div>
              <div>
                <div className='partner-sign'>
                  <img src={JPM} alt=''/>
                </div>
              </div>
              <div>
                <div className='partner-sign'>
                  <img src={lamborg} alt=''/>
                </div>
              </div>
              <div>
                <div className='partner-sign'>
                  <img src={LinkedIn} alt=''/>
                </div>
              </div>
              <div>
                <div className='partner-sign'>
                  <img src={mastercard} alt=''/>
                </div>
              </div>
              <div>
                <div className='partner-sign'>
                  <img src={mersedes} alt=''/>
                </div>
              </div>
              <div>
                <div className='partner-sign'>
                  <img src={PayPal} alt=''/>
                </div>
              </div>
              <div>
                <div className='partner-sign'>
                  <img src={PM} alt=''/>
                </div>
              </div>
              <div>
                <div className='partner-sign'>
                  <img src={porsche} alt=''/>
                </div>
              </div>
              <div>
                <div className='partner-sign'>
                  <img src={TA} alt=''/>
                </div>
              </div>
              <div>
                <div className='partner-sign'>
                  <img src={visa} alt=''/>
                </div>
              </div>
            </Slider>
          </div>
        </div>
      </section>
    )
  }
}

export default withNamespaces()(Partners)