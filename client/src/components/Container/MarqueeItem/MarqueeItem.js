// react
import React from 'react'

// styles
import './MarqueeItem.css'

const MarqueeItem = props => {
  if (props.symbol.length === 6) {
    const startSymbol = props.symbol.slice(0, 3)
    const finishSymbol = props.symbol.slice(-3)
    var pair = `${startSymbol}/${finishSymbol}`
  }
  let grow = props.deltaAbs >= 0 ? 'marquee-growing' : 'marquee-falling'
  return (
    <div className="quote" key={props.bid}>
      <div>
        <p>{pair || props.symbol}</p>

      </div>
      <div className={grow}><p>{props.price} ( {props.delta} )</p></div>

    </div>
  )
}

export default MarqueeItem
