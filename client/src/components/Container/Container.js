// react
import React, {Component} from 'react'
import {t} from 'i18next'
import {withNamespaces} from 'react-i18next'
import Loadable from 'react-loadable'

// hoc
import Layout from '../../hocs/Layout/Layout'
// styles
import './styles/Container.css'
import './styles/Container-animation.css'
import './styles/MediaQueries.css'
import './Crypto/CryptoSection.css'

// actions
import {getQuotes, getStockQuotes} from '../../actions/quotes'
// redux
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
// components
import Marquee from './Marquee/Marquee'
// import Partners from '../Partners/Partners'
// import Reviews from '../Reviews/Reviews'
// import Platforms from '../Platforms/Platforms'
// import Portfolios from '../Portfolios/Portfolios'
// import StocksPanel from './StocksPanel/StocksPanel'
import {cabinet, registration} from '../../const/ApiSetting'
import ContainerLoader from '../LoadingComponent/ContainerLoader'

const Platforms = Loadable({
  loader: () => import('../Platforms/Platforms'),
  loading: ContainerLoader
})

const Reviews = Loadable({
  loader: () => import('../Reviews/Reviews'),
  loading: ContainerLoader
})

const Partners = Loadable({
  loader: () => import('./Partners/Partners'),
  loading: ContainerLoader
})

const Portfolios = Loadable({
  loader: () => import('./Portfolios/Portfolios'),
  loading: ContainerLoader
})

const Crypto = Loadable({
  loader: () => import('./Crypto/Crypto'),
  loading: ContainerLoader
})
const StocksPanel = Loadable({
  loader: () => import('./StocksPanel/StocksPanel'),
  loading: ContainerLoader
})

class Container extends Component {
  componentDidMount () {
    this.props.getQuotes()
    this.props.getStockQuotes()
    setInterval(() => {
      this.props.getQuotes()
      this.props.getStockQuotes()
    }, 90000)
  }

  render () {
    const {quotes} = this.props

    return (
      <Layout container='container'>
        <div className="main-page-container">
          <div className="bkg bkg1"/>
          <div className="bkg bkg2"/>
          <div className="bkg bkg3"/>
          <div className="bkg bkg4"/>
          <div className="bkg bkg5"/>
          <div className="bkg bkg6"/>
          <div className="bkg bkg7"/>
          <div className="bkg bkg8"/>

          <div className="main-title-block">
            <div className="first-line-1">
              <h1>{t('main h-2')}</h1>
            </div>
            <div className="second-line-1">
              <h2>{t('main-p-2.p-1')}</h2>
              <h2>{t('main-p-2.p-2')}</h2>
              <h2>{t('main-p-2.p-3')}</h2>
            </div>
          </div>

          <div className="main-title-block">
            <div className="first-line-2">
              <h1>{t('main h-1')}</h1>
            </div>
            <div className="second-line-2">
              <h2>{t('main-p-1.p-1')}</h2>
              <h2>{t('main-p-1.p-2')}</h2>
              <h2>{t('main-p-1.p-3')}</h2>
            </div>
          </div>

          <div className="main-title-block">
            <div className="first-line-3">
              <h1>{t('main h-3')}</h1>
            </div>
            <div className="second-line-3">
              <h2>{t('main-p-3')}</h2>
            </div>
          </div>

          <div className="main-title-block">
            <div className="first-line-4">
              <h1>{t('main h-4')}</h1>
            </div>
            <div className="second-line-4">
              <h2>{t('main-p-4')}</h2>
            </div>
          </div>

          <a href={registration} className="btn">{t('start trade')}</a>

        </div>
        <div className="marquee-container">
          <Marquee quotes={quotes}/>
        </div>

        <Crypto/>
        <Partners/>
        <Portfolios/>
        <StocksPanel/>
        <Reviews/>
        <Platforms/>

      </Layout>
    )
  }
}

const mapStateToProps = state => ({
  quotes: state.quotes.items,
  stocks: state.stocksReducer.stocks
})
const mapDispatchToProps = dispatch =>
  bindActionCreators({getQuotes, getStockQuotes}, dispatch)

export default withNamespaces()(connect(
  mapStateToProps,
  mapDispatchToProps
)(Container))
