import React from 'react'
import {t} from 'i18next'
import {withNamespaces} from 'react-i18next'
import {Link} from 'react-router-dom'
import './Portfolios.css'

const Portfolios = () => {
  return (
    <div className="portfolios-section">
      <div className="portfolios-bkg bkg-p-1"></div>
      <div className="portfolios-bkg bkg-p-2"></div>
      <div className="portfolios-bkg bkg-p-3"></div>
      <div className="portfolios-bkg bkg-p-4"></div>
      <div className="portfolios-bkg bkg-p-5"></div>
      <div className="portfolio-h-wrap">
        <h1 className="portfolios-header">{t('investment portfolios')}</h1>

        <div className="portfolio-1">
          <h3 className="portfolios-header">{t('portfolios-p-1')}</h3>
          <h3 className="portfolios-header">{t('portfolios-p-2')}</h3>
        </div>

        <div className="portfolio-2">
          <h3 className="portfolios-header">{t('portfolios-p-3')}</h3>
        </div>

        <div className="portfolio-3">
          <h3 className="portfolios-header">{t('portfolios-p-4')}</h3>
        </div>
      </div>

      <Link className="btn portfolios-btn" to={'/investment'}>
        {t('more-info')}
      </Link>
    </div>
  )
}

export default withNamespaces()(Portfolios)