// react
import React from 'react'
import { Link } from 'react-router-dom'
import {t} from 'i18next'

// title info
import DocumentTitle from 'react-document-title'

// semantic ui
import { Button } from 'semantic-ui-react'

// styles
import './NoMatch.css'
import {withNamespaces} from 'react-i18next'

const NoMatch = () => {
  return (
    <DocumentTitle title={t('not-found')}>
      <div className="nomatch">
        <div className="nomatch-content">
          <h1>404</h1>
          <p>{t('not-found')}</p>
          <Link to="/">
            <Button
              content={t('to-main')}
              secondary
              style={{ fontFamily: 'Noto Sans Bold Italic' }}
            />
          </Link>
        </div>
      </div>
    </DocumentTitle>
  )
}

export default withNamespaces()(NoMatch)
