import React, {Component} from 'react'
import {t} from 'i18next'
import {withNamespaces} from 'react-i18next'

import './Platforms.css'
import traderPc from '../../assets/platforms/trader_pc.png'
import traderWeb from '../../assets/platforms/traderweb.png'
import appStore from '../../assets/platforms/topic_apple_logo.png'
import googlePlay from '../../assets/platforms/google-play.png'
import {webTrader} from '../../const/ApiSetting'

class Platforms extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isShow: false
    }
  }

  showInfo () {
    const info = document.getElementById('platforms-info')
    info.style.opacity = '1'
    info.style.transitionDelay = '0'
  }

  hideInfo () {
    const info = document.getElementById('platforms-info')
    info.style.opacity = '0'
    info.style.transitionDelay = '1'
  }

  render () {
    return (
      <section className="platforms">
        <div className="platforms-container">
          <div className="platforms-signs">
            <div className="platform-item">
              <img src={traderPc} alt="" className="platform-img"/>
              <h3>Trader pc</h3>
            </div>
            <div className="platform-item" onMouseEnter={this.showInfo} onMouseLeave={this.hideInfo}>
              <img src={appStore} alt="" className="platform-img"/>
              <h3>app store</h3>
            </div>
            <div className="platform-item" onMouseEnter={this.showInfo} onMouseLeave={this.hideInfo}>
              <img src={googlePlay} alt="" className="platform-img"/>
              <h3>Google play</h3>
            </div>
            <a href={webTrader} className="platform-item">
              <img src={traderWeb} alt="" className="platform-img"/>
              <h3>Trader Web</h3>
            </a>
          </div>
          <div className="platforms-info" id='platforms-info'>
            <p>{t('platforms-information')}</p>
          </div>
        </div>
      </section>
    )
  }
}

export default withNamespaces()(Platforms)