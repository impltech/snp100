// react
import React from 'react'
import {Link} from 'react-router-dom'
import {t} from 'i18next'

// styles
import './Footer.css'
import logo from '../../assets/materials/logo_footer.png'

// translation
import {withNamespaces} from 'react-i18next'
import {Image} from 'semantic-ui-react'
import login from '../../assets/materials/login_icon.png'
import register from '../../assets/materials/reg_icon.png'
import lang from '../../assets/materials/language_icon.png'
import {cabinet, registration, webTrader} from '../../const/ApiSetting'

const Footer = (props) => {
  const logoStyle = {
    width: '25px',
    height: '25px',
    marginRight: '10px'
  }

  const langLogoStyle = {
    height: '24px',
    width: '24px',
    margin: 0
  }

  function showMenu () {
    const menu = document.getElementById('footer-dropdown')
    if (menu) {
      menu.className === 'learning-center-links' ? menu.className = 'learning-center-links-hidden'
        : menu.className = 'learning-center-links'
    }
  }

  function hideMenu () {
    const menu = document.getElementById('footer-dropdown')
    if (menu) {
      menu.className = 'learning-center-links-hidden'
    }
  }

  return (
    <section className='footer-section'>
      <div className="footer-block" onMouseLeave={hideMenu}>

        <div className='footer-logo-block'>
          <div className="footer-logo">
            <Link to='/'>
              <img src={logo} alt=""/>
            </Link>
          </div>

          <div className="law-info-links">
            <Link to="/terms">{t('terms of use')}</Link>
            <Link to="/risk">{t('risk notification')}</Link>
            <Link to="/lawinfo">{t('legal information')}</Link>
          </div>
        </div>

        <div className='footer-upper-links'>
          <div>
            <Link to="/bonus" className='footer-link'>{t('bonuses')}</Link>
          </div>
          <div>
            <Link to="/saleinfo/diff_contracts" className='footer-link'>{t('trading info')}</Link>
          </div>
          <div><Link to="/faq/disputes" className='footer-link'>FAQ</Link></div>
          <div><Link to="/contacts" className='footer-link'>{t('contacts')}</Link></div>
        </div>

        <div className='footer-main-links'>
          <div>
            <div>
              <Link to={'#'} className='footer-link' onClick={showMenu}>{t('learning center')}</Link>
              <div className="learning-center-links-hidden" id='footer-dropdown'>
                <Link to="/stock">{t('stocks')}</Link>
                <Link to="/indexes">{t('indexes')}</Link>
                <Link to="/products">{t('commodity')}</Link>
                <Link to="/forex">{t('forex')}</Link>
                <Link to="/cfd">CFD</Link>
              </div>
            </div>
          </div>
          <div><Link to="/investment" className='footer-link'>{t('investment portfolios')}</Link></div>
          <div><Link to="/calendar" className='footer-link'>{t('economic calendar')}</Link></div>
          <div><Link to="/debentures" className='footer-link'>{t('debentures')}</Link></div>
        </div>

        <div className='footer-login-links'>
          <div><Link to="/accounts" className='footer-link'>{t('accounts-types')}</Link></div>
          <div><a href={webTrader} className='footer-link'>WEBTRADER</a></div>
          <div className='footer-register-link'>
            <Image src={login} alt="login_logo" style={logoStyle}/>
            <a href={cabinet} className='footer-link'>{t('sing in')}</a>
          </div>
          <div className='footer-register-link'>
            <Image src={register} alt="register_logo" style={logoStyle}/>
            <a href={registration} className='footer-link'>{t('registration')}</a>
          </div>
          <div>
            <Image
              src={lang}
              alt="lang_logo"
              className='lang-link'
              style={langLogoStyle}
              onClick={props.modal_open}
            />
          </div>
        </div>

      </div>
      <div className="attention-block">
        <p>{t('caution-p-1')} {' '}
          <Link to={'/risk'}>{t('caution-link-1')} </Link> {' '}
          {t('caution-p-2')}
          <Link to={'/terms'}>{t('caution-link-2')} </Link> {' '}
          {t('caution-p-3')}
        </p>
      </div>
      <div className="site-owned">
        <p>{t('owner-info')}</p>
      </div>
    </section>
  )
}

export default withNamespaces()(Footer)
