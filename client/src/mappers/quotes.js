export default quote => {
  return {
    symbol: quote.symbol,
    timeStamp: quote.timestamp,
    ask: quote.ask,
    bid: quote.bid,
    price: quote.price,
    delta: quote.delta,
    deltaAbs: quote.deltaAbs
  }
}
